# sisop-praktikum-modul-3-2023-AM-B04

## Anggota Kelompok B04:
+ Akbar Putra Asenti Priyanto (5025211004)
+ Muhammad Rafi Sutrisno (5025211167)
+ Muhammad Rafi Insan Fillah (5025211169)

# Soal 1

## Penjelasan Script
## lossless.c

```
#include<stdio.h> 
#include<stdlib.h> 
#include<unistd.h> 
#include<sys/types.h> 
#include<string.h> 
#include<sys/wait.h> 
#include<ctype.h>

#define MAX_TREE_HT 100

```

Pertama include library dan variabel absolut untuk height dari huffman tree yang akan digunakan

```
typedef struct{
    int msglen;
    char msg[1001];
    int regCharCount;
    char regChar[25];
    int charFreq[25];
} msgSend;

typedef struct{
    int msglen;
    char msg[1001][10];
    int regCharCount;
    char regChar[25];
    char regCharCode[25][10];
} msgBack;

```

Selanjutnya membuat struct yang akan digunakan untuk mengirim data ke child process (msgSend) dan mengirim data kembali ke parent process (msgBack). msglen merupakan panjang message yang akan dikirim, msg merupakan string message yang akan dikirim (pada msgBack berupa teks huffman), regCharCount merupakan jumlah character yang distinct, regChar merupakan distinct character yang terdaftar.

Pada msgSend terdapat array charFreq yang merupakan array frekuensi masing-masing registered character sesuai index regChar. Pada msgBack terdapat regCharCode yang merupakan list dari kode huffman pada masing-masing regChar.

```
struct NodeHuffman {
    char data;
    unsigned freq;
    struct NodeHuffman *left, *right;
};

struct MinHeap {
    unsigned size;
    unsigned kapasitas;
 
    struct NodeHuffman** array;
};

struct NodeHuffman* buat_Node(char data, unsigned freq){
    struct NodeHuffman* newNode = (struct NodeHuffman*)malloc(sizeof(struct NodeHuffman));
 
    newNode->left = newNode->right = NULL;
    
    newNode->freq = freq;
    newNode->data = data;
 
    return newNode;
}

struct MinHeap* create_huffmanMinHeap(unsigned kapasitas){
 
    struct MinHeap* MinHeap = (struct MinHeap*)malloc(sizeof(struct MinHeap));
 
    MinHeap->size = 0;
    MinHeap->kapasitas = kapasitas;
 
    MinHeap->array = (struct NodeHuffman**)malloc(MinHeap->kapasitas * sizeof(struct NodeHuffman*));
    return MinHeap;
}

void tukarNode(struct NodeHuffman** a, struct NodeHuffman** b){
 
    struct NodeHuffman* t = *a;
    *a = *b;
    *b = t;
}

void restruktur_MinHeap(struct MinHeap* MinHeap, int idx){
    int cur_small = idx;
    int left = 2 * idx + 1;
    int right = 2 * idx + 2;
 
    if (left < MinHeap->size && MinHeap->array[left]->freq < MinHeap->array[cur_small]->freq)
        cur_small = left;
 
    if (right < MinHeap->size && MinHeap->array[right]->freq < MinHeap->array[cur_small]->freq)
        cur_small = right;
 
    if (cur_small != idx) {
        tukarNode(&MinHeap->array[cur_small], &MinHeap->array[idx]);
        restruktur_MinHeap(MinHeap, cur_small);
    }
}

int isSizeOne(struct MinHeap* MinHeap){
    return (MinHeap->size == 1);
}
 
struct NodeHuffman* extractMin(struct MinHeap* MinHeap){
 
    struct NodeHuffman* temp = MinHeap->array[0];
    MinHeap->array[0] = MinHeap->array[MinHeap->size - 1];
 
    --MinHeap->size;
    restruktur_MinHeap(MinHeap, 0);
 
    return temp;
}

void insertToHeap(struct MinHeap* MinHeap, struct NodeHuffman* NodeHuffman){
 
    ++MinHeap->size;
    int i = MinHeap->size - 1;
 
    while (NodeHuffman->freq < MinHeap->array[(i - 1) / 2]->freq && i) {

        MinHeap->array[i] = MinHeap->array[(i - 1) / 2];
        i = (i - 1) / 2;
    }
 
    MinHeap->array[i] = NodeHuffman;
}

void initMinHeap(struct MinHeap* MinHeap){
    int n = MinHeap->size - 1;
    int i;
 
    for (i = (n - 1) / 2; i >= 0; --i)
        restruktur_MinHeap(MinHeap, i);
}
 
void printCodeArr(int arr[], int n){
    for (int i = 0; i < n; ++i)
        printf("%d", arr[i]);
 
    printf("\n");
}

int isLetterNode(struct NodeHuffman* root){
    return !(root->left) && !(root->right);
}

struct MinHeap* createAndinitMinHeap(char data[], int freq[], int size){
 
    struct MinHeap* MinHeap = create_huffmanMinHeap(size);
 
    for (int i = 0; i < size; ++i)
        MinHeap->array[i] = buat_Node(data[i], freq[i]);
 
    MinHeap->size = size;
    initMinHeap(MinHeap);
 
    return MinHeap;
}
 

struct NodeHuffman* buildHuffmanTree(char data[],
                                     int freq[], int size){
    struct NodeHuffman *left, *right, *top;
    struct MinHeap* MinHeap
        = createAndinitMinHeap(data, freq, size);

    while (!isSizeOne(MinHeap)) {
        left = extractMin(MinHeap);
        right = extractMin(MinHeap);
        top = buat_Node('$', left->freq + right->freq);
 
        top->left = left;
        top->right = right;
 
        insertToHeap(MinHeap, top);
    }
    return extractMin(MinHeap);
}

int assign = 0;
void assignCodes(struct NodeHuffman* root, char arr[], int top, msgBack *data){

    if (root->left) {
        arr[top] = '0';
        assignCodes(root->left, arr, top + 1, data);
    }

    if (root->right) {
        arr[top] = '1';
        assignCodes(root->right, arr, top + 1, data);
    }

    if (isLetterNode(root)) {
        data->regChar[assign] = root->data;
        strcpy(data->regCharCode[assign], arr);

        data->regCharCode[assign][top] = '\0';
        assign++;
    }
}

struct NodeHuffman* HuffmanCodes(char data[], int freq[], int size){
    struct NodeHuffman* root
        = buildHuffmanTree(data, freq, size);
    
    return root;
}

```

Berikutnya fungsi-fungsi pembantu untuk membangun huffman tree menggunakan standard template example.

```
int main() { 

	int fd1[2];
	int fd2[2];
	pid_t p; 

	if (pipe(fd1)==-1) { 
		fprintf(stderr, "Pipe Failed" ); 
		return 1; 
	} 
	if (pipe(fd2)==-1) { 
		fprintf(stderr, "Pipe Failed" ); 
		return 1; 
	} 

	p = fork(); 

	if (p < 0) { 
		fprintf(stderr, "fork Failed" ); 
		return 1; 
	} 


```
Pada fungsi main dilakukan inisiasi pipe dan fork process menjadi parent dan child.

```
// Parent process 
	else if (p > 0) { 

		close(fd1[0]); // Close reading end of first pipe 

        /////////////////// count sect //////////////////////////////

        msgSend data;

        char res[1001];
        int  i,j,k,count=0,n;
        char ch;

        FILE  *fpin;
        fpin = fopen("file.txt", "r");
        int tl=0;
        while((ch=getc(fpin))!=EOF){
            char buf[2];
            if((ch>='a' && ch<='z') | (ch>='A' && ch<='Z')){
                buf[0] = toupper(ch);
                strcat(res, buf);
                tl++;
            }
            else if(ch == ' '){
                strcat(res, " ");
                tl++;
            }
        }
        data.msglen = strlen(res);
        strcpy(data.msg, res);

        fclose(fpin);

        long len = strlen(res);
        int regCharCount = -1;
        for(i=0;i<len;i++){
            count=1;
            if(res[i] == '\0'){continue;}

            // jika distinct char
            regCharCount++;
            for(j=i+1;j<len;j++) {   
                if(res[i]==res[j]) {
                    count++;
                    res[j]='\0';
                }
            }
            data.regChar[regCharCount] = res[i];
            data.charFreq[regCharCount] = count;
        }
        data.regCharCount = regCharCount+1;

        if(write(fd1[1], &data, sizeof(msgSend))<0){printf("error2\n");return 2;}
        msgBack data4;
        /////////////////// count sect //////////////////////////////

		close(fd1[1]);

```

Pada parent process dilakukan inisiasi struct msgSend pertama dan baca file.txt per-character untuk dilakukan pengubahan dan filtering character menjadi lowercase dan hanya alfabet (pada kasus ini whitespace include). Setelah didapat character hasil filtering (res) maka copy res ke data.msg dan strlen(res) ke data.msglen. Lalu dilakukan perhitungan frekuensi distinct character dan dimasukkan ke data.regCharCount, data.regChar, dan data.regCharFreq. Setelah data lengkap maka kirim melalui pipe ke child process.

```
// child process 
	else{ 
		close(fd1[1]); // Close writing end of first pipe 
        
        /////////////////// recieve child sect ///////////////////////
		
        msgSend data2;
        msgBack data3;

        if(read(fd1[0], &data2, sizeof(msgSend))<0){printf("error3\n");return 3;}

        struct NodeHuffman* bufroot = HuffmanCodes(data2.regChar, data2.charFreq , data2.regCharCount);
        
        char arr[MAX_TREE_HT];
        int top = 0;
        assignCodes(bufroot, arr, top, &data3);

        data3.msglen = data2.msglen;

        for(int i=0; i<data2.msglen; i++){
            for(int j=0; j<24; j++){
                if(data2.msg[i] == data3.regChar[j]){
                    strcpy(data3.msg[i], data3.regCharCode[j]);
                }
            }
        }

		/////////////////// recieve child sect ///////////////////////
		close(fd1[0]); 
		close(fd2[0]); 

		/////////////////// send back sect ////////////////////////////
        if(write(fd2[1], &data3, sizeof(msgBack))<0){printf("error4\n");return 4;}
		/////////////////// send back sect ////////////////////////////

		close(fd2[1]); 

		exit(0); 
	} 
```

Pada child process menerima data yg dikirim menggunakan struct msgSend data2, dari data2.regChar, data2.charFreq, dan data2.regCharCount tersebut dapat digunakan untuk build huffman tree. Setelah didapat root node dari huffman tree maka akan ditraverse tree tersebut untuk di-list kode huffman masing-masing character pada fungsi assignCodes(). Setelah didapat list huffman code maka dilakukan enkripsi, enkripsi dilakukan dengan mencocokkan data2.msg[i] dengan index list data3.regChar[j], jika sesuai maka strcpy data3.msg[i] dari data3.regCharCode[j] untuk assign huffman code pada character tersebut. Setelah data3 lengkap maka dikirim kembali ke parent process.

```
wait(NULL);

		close(fd2[1]);

        ////////////////// recieve back sect //////////////////////////

        if(read(fd2[0], &data4, sizeof(msgBack))<0){printf("error5\n");return 5;}

        ////////////////// recieve back sect //////////////////////////

        int bitCount = 0;
        for(int i=0; i<data4.msglen; i++){
            bitCount += strlen(data4.msg[i]);
        }

        printf("\nencryption:\n");
        for(int i=0; i<data4.msglen; i++){
            printf("%s ", data4.msg[i]);
        }
        printf("\n\nDecryption:\n");

        for(int i=0; i<data4.msglen; i++){
            for(int j=0; j<24; j++){
                if(!strcmp(data4.msg[i], data4.regCharCode[j])){
                    printf("%c", data4.regChar[j]);
                }
            }
        }
        printf("\n\n");

        printf("total huruf: %d\nhuffman bit: %d\nASCII bit: %d\n", data4.msglen, bitCount, data4.msglen*8);

		close(fd2[0]); 
	} 

```

Selanjutnya pada parent process menerima data via struct msgSend data4. Pertama hitung bit masing-masing character pada data4.msg dengan strlen masing-masing character. Sebagai pembanding akan ditampilkan msg yang dienkripsi berupa teks huffman. Selanjutnya dekripsi msg dengan pencocokan data4.msg[i] dengan data4.regCharCode[j]. Setelah hasil dekripsi ditampilkan akan dibandingkan bit yang digunakan pada teks huffman dengan teks ASCII biasa, perhitungan bit pada teks ASCII cukup dengan data4.msglen*8.
 

# Soal 2

## Penjelasan Script
## kalian.c
```
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>


#define ROWS 4
#define COLS 5
#define SHM_SIZE (sizeof(int) * ROWS * COLS)
```
Pertama include library yang akan digunakan lalu define beberapa variabel yang akan dipakai nanti. ROWS dan COLS untuk baris dan kolom dari matriks hasil, SHM_SIZE untuk size dari shared memory.
```
int main(){
	
	int array1[4][2], array2[2][5], hasil[4][5];
	srand(time(0));
	for(int i = 0; i < 4; i++){
		for(int j = 0; j < 2; j++){
			array1[i][j] = (rand() % (5 - 1 + 1)) + 1;
		}
	}
	for(int i = 0; i < 2; i++){
		for(int j = 0; j < 5; j++){
			array2[i][j] = (rand() % (4 - 1 + 1)) + 1;
		}
	}
```
Selanjutnya buat 3 array 2 dimensi dengan ukuran masing masing yang telah ditentukan dengan satu array sebagai array yang menyimpan matriks hasil perkalian. Lalu untuk 2 array pertama isi dengan angka random dengan interval yang telah ditentukan.
```
	for(int i = 0; i < 4; i++){
		for(int j = 0; j < 5; j++){
			hasil[i][j] = 0;
			for(int k = 0; k < 2; k++){
				hasil[i][j] += array1[i][k] * array2[k][j];
			}
		}
	}
```
Selanjutnya hitung perkalian matriks dengan cara mengkali masing masing baris matriks pertama dengan kolom matriks kedua lalu tambahkan sesuai dengan posisinya. 
```
	for(int i = 0; i < 4; i++){
		for(int j = 0; j < 2; j++){
			printf("%d ", array1[i][j]);
		}
		printf("\n");
	}
	printf("\n");
	for(int i = 0; i < 2; i++){
		for(int j = 0; j < 5; j++){
			printf("%d ", array2[i][j]);
		}
		printf("\n");
	}
	printf("\nHasil Matriks : \n");
	for(int i = 0; i < 4; i++){
		for(int j = 0; j < 5; j++){
			printf("%d ", hasil[i][j]);
		}
		printf("\n");
	}
```
Selanjutnya kode diatas untuk mengeprint semua matriks tadi.
```
	key_t randomkey = 123456;
	int shm_id = shmget(randomkey, SHM_SIZE, 0666|IPC_CREAT);
	
	int *shm = (int *) shmat(shm_id, NULL, 0);
	if (shm == (int *) -1) {
	    perror("shmat");
	    exit(1);
	}

	int (*shared_array)[COLS] = (int (*)[COLS]) shm;
```
Selanjutnya buat key yang digunakan untuk melakukan shared memory dengan program cinta.c. Setelah itu buat shared memory dengan menggunakan key tadi dan size yang telah ditentukan di awal tadi. Size nya berukuran sebesar jumlah array hasil.
```
	key_t randomkey2 = 1234567;
	int shm_id2 = shmget(randomkey2, SHM_SIZE, 0666|IPC_CREAT);
	
	int *shm2 = (int *) shmat(shm_id2, NULL, 0);
	if (shm2 == (int *) -1) {
	    perror("shmat");
	    exit(1);
	}

	int (*shared_array2)[COLS] = (int (*)[COLS]) shm2;

```
Selanjutnya buat key lagi yang digunakan untuk melakukan shared memory dengan program sisop.c. Setelah itu buat shared memory dengan cara yang sama dengan sebelumnya.
```

	for (int i = 0; i < ROWS; i++) {
	    for (int j = 0; j < COLS; j++) {
	    	shared_array[i][j] = hasil[i][j];
	        shared_array2[i][j] = hasil[i][j];
	    }
	}
```
Selanjutnya write data ke kedua shared array tadi dengan data yang berisi hasil perkalian matriks.
```
	sleep(10);
	shmdt(shared_array);
	shmctl(shm_id, IPC_RMID, NULL);
	shmdt(shared_array2);
	shmctl(shm_id2, IPC_RMID, NULL);
	
	return 0;
}
```
Selanjutnya sleep 10 detik untuk memberi waktu menjalankan kedua program lain. lalu jika sudah maka shared array akan selesai.

## cinta.c

```
#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>
#include <pthread.h>
#include <time.h>

#define ROWS 4
#define COLS 5
#define SHM_SIZE (sizeof(int) * ROWS * COLS)
```
Pertama include library yang akan digunakan lalu define beberapa variabel yang akan dipakai nanti. ROWS dan COLS untuk baris dan kolom dari matriks hasil, SHM_SIZE untuk size dari shared memory.
```
int array[ROWS*COLS];
int result[ROWS*COLS];
```
Selanjutnya disini saya akan menyimpan array dari shared memory ke array 1 dimensi dan menyimpan hasil faktorial ke array result 1 dimensi. 
```
void *faktorial(void *no){
        int *k_num = (int *)(no);
        long long factor = 1;
        for(int i = array[*k_num]; i > 0; i--){
            factor *= i;
        }
        result[*k_num] = factor;
        
        long k = 1000000000;
        while(k--){}
}
```
Selanjutnya buat fungsi yang akan digunakan thread. Fungsi tersebut adalah fungsi faktorial. Fungsi tersebut menerima 1 argumen beruba integer yang merupakan index array. Setelah itu akan dicari faktorial dari array matriks sesuai index dan jika sudah nilai tersebut ditaruh di array result sesuai indexnya.
While(k--) digunakan untuk memperjelas perbedaan waktu antara program yang menggunakan thread dengan yang tidak.
```
int main(){

        key_t randomkey = 123456;
        int shm_id = shmget(randomkey, SHM_SIZE, 0666|IPC_CREAT);

        int *shm = (int *) shmat(shm_id, NULL, 0);
        if (shm == (int *) -1) {
            perror("shmat");
            exit(1);
        }
```
Pada main() pertama pasangkan shared memory menggunakan key yang sama pada program kalian.c dan ukuran yang sama dengan shared memory.
```		
        int (*shared_array)[COLS] = (int (*)[COLS]) shm;
		int k = 0;
        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < COLS; j++) {
                array[k] = shared_array[i][j];
                k++;
            }
        }
        shmdt(shared_array);
        shmctl(shm_id, IPC_RMID, NULL);
```
Setelah itu read nilai dari shared memory tersebut ke array. 
```		
		int a=0;
        printf("\nHasil Perkalian Matriks : \n");
        for(int i = 0; i < 20; i++){               
            printf("%d ", array[i]);
            a++;
			if(a==5){
               printf("\n");
               a = 0;
        	}
        }
        printf("\n");
```
Selanjutnya print array tadi yang berisi data shared memory.
```
        int jumlah = ROWS*COLS;
        pthread_t thread[jumlah];
```
Selanjutnya buat thread sejumlah banyaknya angka pada matriks.
```        
        int no[jumlah];
        for(int i = 0; i < 20; i++){
        	no[i]=i;
		}
```
Lalu loop diatas digunakan untuk membuat array no yang digunakan sebagai index pada array.
```
        for(int i = 0; i < jumlah; i++){
            pthread_create(&thread[i], NULL, faktorial, (void*) &no[i]);
        }
        for(int k = 0; k < jumlah; k++){
            pthread_join(thread[k], NULL);
        }
```
Selanjutnya create thread yang menggunakan fungsi faktorial dan menerima argumen index.
```	
		a = 0;
		printf("\nHasil Faktorial Matriks : \n");
        for(int i = 0; i < 20; i++){               
            printf("%d ", result[i]);
            a++;
			if(a==5){
               printf("\n");
               a = 0;
        	}
        }
        return 0;
}
```
Terakhir print hasil faktorial matriks dalam bentuk matriks.

## sisop.c

Pada program ini kurang lebih sama dengan program cinta.c hanya saja tidak menggunakan thread.

```
#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>
#include <time.h>


#define ROWS 4
#define COLS 5
#define SHM_SIZE (sizeof(int) * ROWS * COLS)
```
Include library yang digunakan lalu difine rows cols dan size dari shared memory.
```
int faktorial(int var){
        long long factor = 1;
        for(int i = var; i > 0; i--){
            factor *= i;
        }
        long k = 1000000000;
        while(k--){}
        return factor;
}
```
Lalu buat fungsi untuk menghitung faktorial dari angka yang akan dimasukkan.
While(k--) digunakan untuk memperjelas perbadaan waktu antara program yang menggunaka thread dengan yang tidak.
```
int main(){

        int array[ROWS][COLS];

        key_t randomkey = 1234567;
        int shm_id = shmget(randomkey, SHM_SIZE, 0666|IPC_CREAT);

        int *shm = (int *) shmat(shm_id, NULL, 0);
        if (shm == (int *) -1) {
            perror("shmat");
            exit(1);
        }
```
Pada main() pertama pasang shared memory menggunakan key kedua yang sesuai dengan yang ada pada program kalian.c dan size yang sama dengan shared memory.
```
        int (*shared_array)[COLS] = (int (*)[COLS]) shm;

        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < COLS; j++) {
                array[i][j] = shared_array[i][j];
            }
        }
        shmdt(shared_array);
        shmctl(shm_id, IPC_RMID, NULL);
```
Lalu read data dari shared memory dan masukkan ke array.
```
        printf("\nHasil Perkalian Matriks : \n");
        for(int i = 0; i < 4; i++){
                for(int j = 0; j < 5; j++){
                        printf("%d ", array[i][j]);
                }
                printf("\n");
        }
        printf("\n");
```
Lalu print data yang telah di read dari shared memory tadi.
```
        long long factor;
        int num;
        for(int i = 0; i < 4; i++){
                for(int j = 0; j < 5; j++){
                        array[i][j] = faktorial(array[i][j]);
                }
        }
```
Lalu ubah semua data dari array tadi menjadi nilai faktorialnya.
```
		printf("\nHasil Faktorial Matriks : \n");
        for(int i = 0; i < 4; i++){
                for(int j = 0; j < 5; j++){
                        printf("%d ", array[i][j]);
                }
                printf("\n");
        }
        printf("\n");
        
        return 0;
}
```
Terakhir print array tersebut lagi, maka array tersebut akan berisi nilai faktorial dari masing masing angka.

## Perbedaan cinta.c dan sisop.c

![image](https://github.com/Rafi-Sutrisno/Tugas-PBO/blob/main/SS%20thread%20vs%20no%20Thread.png?raw=true)

Pada contoh diatas perbedaan antara program yang menggunakan multithread dengan yang tidak adalah waktunya dimana jika menggunakan thread maka waktu yang dihasilkan lebih cepat. Hal itu disebabkan karena jika menggunakan multithread maka akan ada banyak thread yang dikerjakan secara bersamaan sedangkan jika tidak menggunakan multithread maka semua proses akan dikerjakan satu satu yang akan memakan waktu lebih lama.

# Soal3

## Penjelasan Script
## stream.c
```
#define _GNU_SOURCE

#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <json-c/json.h>
#include <semaphore.h>
#include <regex.h>
#include <sys/ipc.h>
#include <sys/msg.h>
```
Berikut adalah berbagai library yang akan digunakan, perlu diketahui bahwa ```stream.c``` menggunakan tambahan library jsonc untuk memproses file .json dengan lebih mudah. Untuk instalasi menggunakan ```sudo apt install libjson-c-dev``` dan untuk kompilasi program menggunakan ```gcc stream.c -ljson-c -o <file output>```.
```
// structure for message queue
struct mesg_buffer {
    long mesg_type;
    int user_pid;
    char mesg_text[1024];
} message;
```
Berikut adalah struktur dari message yang akan dikirim dari ```user.c``` ke ```stream.c```, data yang dikirim berupa pid dari proses ```user.c``` yang berjalan dan string yang menyimpan command untuk ```stream.c```.
```
const char base64_map[] =   {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                            'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                            'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                            'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'};
```
Berikut adalah array yang digunakan untuk decode lagu dari base64.
```
int countinplaylist(char *fname, char *str){ // Fungsi untuk menghitung banyak line yang mengandung input di playlist.txt
    FILE *fp;
    int find_count = 0;
    char temp[1024];

    if((fp = fopen(fname, "r")) == NULL){ // Membuka playlist.txt
        return(-1);
    }

    while(fgets(temp, sizeof(temp), fp) != NULL){
        if((strlen(temp) > 0) && (temp[strlen(temp) - 1] == '\n')) temp[strlen(temp) - 1] = '\0'; // Membandingkan apakah di line terdapat input (case insensitive)
        if((strcasestr(temp, str)) != NULL) find_count++; // Increment jika menemukan
    }

    if(fp){
        fclose(fp);
    }

    return find_count;
}
```
Fungsi yang digunakan untuk menghitung banyaknya lagu yang mengandung input di playlist.txt, menggunakan ```strcasestr``` untuk mencari string yang mengandung input di playlist.txt.
```
int findinplaylist(char *fname, char *str){ // Fungsi untuk mencari secara eksak sesuai input
    FILE *fp;
    int find_count = 0;
    char temp[1024];

    if((fp = fopen(fname, "r")) == NULL){ // Membuka playlist.txt
        return(-1);
    }

    while(fgets(temp, sizeof(temp), fp) != NULL){ // Membandingkan dengan input
        if((strlen(temp) > 0) && (temp[strlen(temp) - 1] == '\n')) temp[strlen(temp) - 1] = '\0';
        if((!strcasecmp(temp, str))) find_count++; // Membandingkan apakah line sama dengan input
    }

    if(fp){
        fclose(fp);
    }

    return find_count;
}
```
Fungsi untuk mencari lagu sesuai dengan input di playlist.txt, menggunakan ```strcasecmp``` untuk mencari string yang sama dengan input di playlist.txt.
```
void base64_decode(char* str) { // Fungsi untuk decode base64
    char counts = 0;
    char buffer[4];
    char* plain = malloc(strlen(str) * 3 / 4 + 1);
    int i = 0, p = 0;

    for(i = 0; str[i] != '\0'; i++) {
        char k;
        for(k = 0 ; k < 64 && base64_map[k] != str[i]; k++);
        buffer[counts++] = k;
        if(counts == 4) {
            plain[p++] = (buffer[0] << 2) + (buffer[1] >> 4);
            if(buffer[2] != 64) plain[p++] = (buffer[1] << 4) + (buffer[2] >> 2);
            if(buffer[3] != 64) plain[p++] = (buffer[2] << 6) + buffer[3];
            counts = 0;
        }
    }

    plain[p] = '\0';   
    strcpy(str, plain);
}
```
Berikut adalah fungsi yang digunakan untuk melakukan decode base64. Encoding Base64 dilakukan dengan cara mengubah karakter menjadi bentuk ascii dan mengubah kode ascii tersebut ke binary 8 bit. Lalu setiap 24 bit dijoinkan dalam sebuah buffer kemudian untuk setiap 6 bit dilakukan translasi ke kode ascii yang baru (menghasilkan 4 karakter), sehingga untuk proses decode setiap 4 karakter akan diubah ke ascii lalu binary dan akan didecode per 8 bit menjadi karakter yang sebenarnya.
```
void rot13_decode(char *str){
    char c;
    while(*str){ // Fungsi untuk decode rot13
        c = *str;
        if(isalpha(c)){ // Untuk a hingga m ditambah 13 karakter
            if(c >= 'a' && c <= 'm' || c >= 'A' && c <= 'M')*str += 13;
            else *str -= 13; // Selain itu, dikurang 13
        } str++;
    }
}
```
Berikut adalah fungsi yang digunakan untuk melakukan decode rot13, proses decode dilakukan dengan menggeser karakter sebesar 13 karakter ke depan atau ke belakang sesuai dengan posisinya di alfabet.
```
void hex_decode(char *str){
    char string[1024];
    int len = strlen(str);
    for(int i = 0, j = 0; j < len; ++i, j += 2){ // Fungsi untuk decode hex
        int val[1];
        sscanf(str + j, "%2x", val);
        string[i] = val[0];
        string[i + 1] = '\0';
    }
    strcpy(str, string);
}
```
Berikut adalah fungsi yang digunakan untuk melakukan decode hex. Dilakukan dengan cara mengambil setiap dua karakter dan mengubahnya dari hex valuenya ke string.
```
void sort(){
    char command[50];
    strcpy(command, "sort -u playlist.txt -o playlist.txt"); // -u menghapus duplikat
    system(command);
}
```
Berikut adalah fungsi untuk melakukan sorting di file playlist.txt, -u digunakan untuk menghapus duplikat.
```
void decrypt(){
    const char *fp = "song-playlist.json";

    struct json_object *parsed_json;
    
    parsed_json = json_object_from_file(fp); // Mengambil file song-playlist.json

    if(!json_object_is_type(parsed_json, json_type_array)){ // Mengecek apakah json berupa array
        printf("Array not Found!\n");
        return;
    }

    int array_len = json_object_array_length(parsed_json);

    for(int i = 0; i < array_len; i++){
        struct json_object *item = json_object_array_get_idx(parsed_json, i); // Untuk setiap objek di array json
        if(!json_object_is_type(item, json_type_object)){ // Mengecek apakah berupa tipe objek
            printf("Item not Found!\n");
            continue;
        }

        struct json_object *method, *song;
        if(!json_object_object_get_ex(item, "method", &method)){ // Mencari method dari objek tersebut
            printf("Encryption Not Found!\n");
            continue;
        }

        if(!json_object_object_get_ex(item, "song", &song)){ // Mencari song dari objek tersebut
            printf("Song Not Found!\n");
            continue;
        }

        char songName[json_object_get_string_len(song) + 1]; // Deklarasi songName sebagai tempat menyimpan song
        strcpy(songName, json_object_get_string(song)); // Mengcopy dari json ke songName

        if(strstr("rot13",json_object_get_string(method))) rot13_decode(songName); // Jika method rot13
        else if(strstr("hex", json_object_get_string(method))) hex_decode(songName); // Jika method hex
        else if(strstr("base64", json_object_get_string(method))) base64_decode(songName); // Jika method base64
        
        char cmd[strlen(songName) + 30];
        snprintf(cmd, sizeof(cmd), "echo \"%s\" >> playlist.txt", songName); // Membuat command untuk menambahkan ke playlist.txt
        system(cmd);
        printf("DECRYPTED:\nMethod: %s\nSong: %s\n\n", json_object_get_string(method), songName);
    }
    
    json_object_put(parsed_json); // Melepas json

    sort(); // Melakukan pengurutan di playlist.txt (Tanpa duplikat)
}
```
Fungsi decrypt() digunakan untuk melakukan dekripsi file ```song-playlist.json```, fungsi ini menggunakan library tambahan jsonc. Cara kerjanya adalah fungsi ini akan mengambil array of objek dari file ```song-playlist.json``` dan akan mencari ```method``` dan ```song``` yang dimiliki oleh setiap objek di file. Kemudian akan mengecek method yang ditemukan dan melakukan fungsi decode sesuai dengan ```method``` tersebut dengan input berupa ```song```. Hasilnya kemudian akan di-echo ke ```playlist.txt``` dan akan dilakukan sorting di akhir fungsi untuk mengurutkan dan menghapus duplikat
```
void add(int uid, char *in){
    char playlist[] = "playlist.txt";
    char add_arg[strlen(in)];
    strcpy(add_arg, in);
    
    if(findinplaylist(playlist, add_arg) < 1){ // Fungsi add perlu mencari secara eksak
        char cmd[strlen(add_arg) + 30];
        snprintf(cmd, sizeof(cmd), "echo \"%s\" >> playlist.txt", add_arg); // Jika tidak ditemukan maka input ditambah ke playlist.txt
        system(cmd);
        sort();

        printf("USER \"%d\" ADD %s\n", uid, add_arg);
    } else {
        printf("SONG ALREADY ON PLAYLIST\n");
    }
}
```
Berikut adalah fungsi yang dijalankan ketika ```stream``` dijalankan command ```ADD <Nama Lagu>```. Sistem akan melakukan pencarian di playlist.txt dan apabila tidak ditemukan lagu tersebut maka akan menambahkannya ke ```playlist.txt```, sedangkan bila lagu tersebut sudah ada di playlist, maka akan mengoutput ```SONG ALREADY ON PLAYLIST```.
```
void play(int uid, char *in){
    char playlist[] = "playlist.txt";
    char play_arg[strlen(in) + 1];
    strcpy(play_arg, in);

    int found = 0;
    found = countinplaylist(playlist, play_arg); // Menghitung banyaknya line yang mengandung input di playlist.txt
    if(found == 0){
        printf("THERE IS NO SONG CONTAINING \"%s\"\n", play_arg); // Jika tidak ada input di playlist
    } else if(found == 1){ // Jika menemukan hanya satu di playlist
        FILE *fp;
        char temp[1024];
        char out[1025];

        if((fp = fopen(playlist, "r")) == NULL){
            return;
        }

        while(fgets(temp, sizeof(temp), fp) != NULL){
            if((strlen(temp) > 0) && (temp[strlen(temp) - 1] == '\n')) temp[strlen(temp) - 1] = '\0'; // Membandingkan apakah di line terdapat input (case insensitive)
            if((strcasestr(temp, play_arg)) != NULL) {
                strcpy(out, temp);
            }
        }

        if(fp){
            fclose(fp);
        }
        
        printf("USER \"%d\" PLAYING %s\n", uid, out);
    } else { // Jika ditemukan lebih dari satu
        printf("THERE ARE \"%d\" SONGS CONTAINING \"%s\"\n", found, play_arg);
        FILE *fp;
        int count = 1;
        char temp[1024];

        if((fp = fopen(playlist, "r")) == NULL){
            return;
        }

        while(fgets(temp, sizeof(temp), fp) != NULL){
            if(strcasestr(temp, play_arg) != NULL){ // Diprint dengan diberi urutan
                printf("%d. %s", count, temp);
                count++;
            }
        }

        if(fp){
            fclose(fp);
        }
    }

}
```
Berikut adalah fungsi yang dipanggil ketika ```stream``` diberi command ```PLAY <Nama Lagu>```. Sistem akan mencari baris yang mengandung nama lagu tersebut di playlist.txt dan akan memainkannya jika ditemukan hanya satu. Bila ditemukan lebih dari satu maka sistem akan mengoutput ```THERE ARE "N" SONGS CONTAINING <Nama Lagu>``` beserta lagu-lagu yang ditemukan. Apabila tidak ditemukan maka sistem mengoutput ```THERE IS NO SONG CONTATINING <Nama Lagu>```
```
void list(){
    sort();
    char command[50];
    strcpy(command, "cat playlist.txt");
    system(command);
    printf("\n");
}
```
Berikut adalah fungsi yang dipanggill ketika ```stream``` diberi command ```LIST```. Sistem akan menampilkan semua line di playlist.txt dengan bantuan ```cat```.
```
int main(){
    regex_t decrypt_regex, list_regex, add_regex, play_regex;
    
    const char *decrypt_pattern = "^DECRYPT\n$"; // Pola decrypt yang diperbolehkan
    const char *list_pattern = "^LIST\n$"; // Pola list yang diperbolehkan
    const char *add_pattern = "^ADD (.+)\n$"; // Pola add yang diperbolehkan
    const char *play_pattern = "^PLAY (.+)\n$"; // Pola play yang diperbolehkan
    
    int ret = regcomp(&decrypt_regex, decrypt_pattern, REG_EXTENDED); // Kompilasi regex
    ret |= regcomp(&list_regex, list_pattern, REG_EXTENDED);
    ret |= regcomp(&add_regex, add_pattern, REG_EXTENDED);
    ret |= regcomp(&play_regex, play_pattern, REG_EXTENDED);
```
Berikut adalah fungsi main() dari program, disini menggunakan regex (regular expression) untuk menyimpan pola-pola yang akan dibaca oleh sistem.
```    
    key_t key;
    int msgid;
  
    key = ftok("stream_playlist", 50);
  
    // setup semaphore
    sem_unlink("/user");
    sem_unlink("/stream");

    sem_t *sem_stream = sem_open("/stream", O_CREAT | O_EXCL, 0660, 0); // Semaphore sem_stream diinisialisasi 0
    if(sem_stream == SEM_FAILED){
        perror("sem_open/stream");
        exit(EXIT_FAILURE);
    }
    
    sem_t *sem_user = sem_open("/user", O_CREAT | O_EXCL, 0660, 2); // Semaphore sem_user diinisialisasi 2
    if(sem_user == SEM_FAILED){
        perror("sem_open/user");
        exit(EXIT_FAILURE);
    }
    
    msgid = msgget(key, 0666 | IPC_CREAT);
```
Selanjutnya dilakukan setup dari IPC yang digunakan dalam ```stream``` berupa message queue dan dua buah semaphore, dimana semaphore stream diinisialisasi dengan nilai 0 dan semaphore user akan diinisialisasi dengan nilai 2 (karena sistem hanya memperbolehkan maksimal dua user untuk berinteraksi dengan ```stream```).
```    
    while(1){
        
        sem_wait(sem_stream); // Menunggu pesan dari user

        msgrcv(msgid, &message, sizeof(message), 1, 0); // Menerima message
        
        printf("USER \"%d\" SENT DATA %s\n", message.user_pid, message.mesg_text); // Display message

        regmatch_t match[2];
        
        ret = regexec(&decrypt_regex, message.mesg_text, 0, NULL, 0); // Membandingkan input dengan regex
        if(ret == 0){ // Jika input berupa decrypt
            decrypt();
        } else {
            ret = regexec(&list_regex, message.mesg_text, 0, NULL, 0); // Jika input berupa list
            if(ret == 0) {
                list();
            } else { // Jika input berupa add
                ret = regexec(&add_regex, message.mesg_text, 2, match, 0);
                if(ret == 0){
                    char arg[match[1].rm_eo - match[1].rm_so + 1]; // Mengambil argumen dari input
                    memcpy(arg, message.mesg_text + match[1].rm_so, match[1].rm_eo - match[1].rm_so);
                    arg[match[1].rm_eo - match[1].rm_so] = '\0';
                    add(message.user_pid, arg);
                } else { // Jika input berupa play
                    ret = regexec(&play_regex, message.mesg_text, 2, match, 0);
                    if(ret == 0){
                        char arg[match[1].rm_eo - match[1].rm_so + 1]; // Mengambil argumen dari input
                        memcpy(arg, message.mesg_text + match[1].rm_so, match[1].rm_eo - match[1].rm_so);
                        arg[match[1].rm_eo - match[1].rm_so] = '\0';
                        play(message.user_pid, arg);
                    } else { // Error message
                        printf("UNKNOWN COMMAND!\n");
                    }
                }
            }
        }
        sem_post(sem_user); // Sinyal ke user bahwa message sudah diproses
    }
    return 0;
}
```
Berikut adalah while loop utama dari sistem, diawali dengan sem_wait pada sem_stream untuk menunggu sinyal dari user kemudian msgrcv untuk menerima pesan dari user. Setelah itu dilakukan regexec untuk mengecek isi command dari user dan melakukan command tersebut beserta mengambil argumennya pada command ```ADD``` dan ```PLAY```. Lalu diakhiri dengan sem_post pada sem_user untuk memberi sinyal ke user bahwa message telah diproses.

## user.c
```
// C Program for Message Queue (Writer Process)
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <semaphore.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/msg.h>

#define MAX 1024
```
Berikut adalah library yang akan digunakan dalam ```user.c```
```  
// structure for message queue
struct mesg_buffer {
    long mesg_type;
    int user_pid;
    char mesg_text[1024];
} message;
```
Berikut adalah struktur message yang digunakan, struktur ini sama dengan struktur pada ```stream.c```
```  
int main(){
    key_t key;
    int msgid;
  
    key = ftok("stream_playlist", 50); // ftok membuat key unique
  
    msgid = msgget(key, 0666 | IPC_CREAT); // msgget membuat message queue dan identifier
    message.mesg_type = 1;
    message.user_pid = getpid();

    sem_t *sem_stream = sem_open("/stream", 0); // Untuk mengakses semaphore sem_stream
    if(sem_stream == SEM_FAILED){
        perror("sem_open/stream");
        exit(EXIT_FAILURE);
    }

    sem_t *sem_user = sem_open("/user", 0); // Untuk mengakses semaphore sem_user
    if(sem_user == SEM_FAILED){
        perror("sem_open/user");
        exit(EXIT_FAILURE);
    }
```
Berikut adalah fungsi main() dari ```user.c```, diawali dengan setup IPC berupa message queue dan dua buah semaphore dari ```stream.c```
```    
    int sem_val;
    sem_getvalue(sem_user, &sem_val);

    if(sem_val == 0){ // Jika semaphore sem_user penuh, maka tidak diperbolehkan masuk
        printf("STREAM SYSTEM OVERLOAD\n");
        return 1;
    }
```
Selanjutnya dilakukan pengecekan nilai semaphore dari sem_user, apabila masih ada posisi kosong (nilai semaphore bukan 0), maka program dapat berlanjut, jika tidak maka akan output ```STREAM SYSTEM OVERLOAD```.
```
    while(1){
        printf("Waiting...\n");

        sem_wait(sem_user);

        printf("Insert Data:");
        fgets(message.mesg_text,MAX,stdin); // Menginput pesan
        
        if(strcmp("EXIT\n", message.mesg_text) == 0){ // Jika pesan EXIT maka keluar program user
            sem_post(sem_user);
            return 0;
        }
        
        msgsnd(msgid, &message, sizeof(message), 0); // Mengirim melalui message queue
        
        printf("Data sent: %s \n", message.mesg_text); // Display 

        sem_post(sem_stream); // Sinyal stream untuk memproses message
    }
  
    return 0;
}
```
Selanjutnya terdapat while loop untuk mengirim command, pada awal loop program akan melakukan sem_wait pada sem_user dan kemudian user dapat menginput data. Apabila data yang diinput berupa ```EXIT``` maka program user akan keluar dan melakukan sem_post pada sem_user untuk melakukan increment (memberi sinyal bahwa ada tempat yang kosong), selain ```EXIT``` maka input dari user akan dikirim ke message queue dan diakhiri dengan sem_post dari sem_stream unuk memberi sinyal ke ```stream``` untuk memproses messsage dari user.
# Soal4
## unzip.c 
```
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
```

Pertama include library yang akan digunakan.

```
int main() {
  pid_t child_id;
  int status;

  child_id = fork();

  if (child_id < 0) {
    exit(EXIT_FAILURE);
  }

  if (child_id == 0) {
    char *argv[] = {"wget", "-O", "./hehe.zip","https://drive.google.com/uc?id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp&export=download", NULL};
    execv("/bin/wget", argv);
    char *mkdir[] = {"mkdir", "./hehe/", NULL};
    execv("/bin/mkdir", mkdir);
    exit(0);
  } else {
    wait(NULL);
    char *argv1[] = {"unzip", "hehe.zip", "-d", "./hehe/",  NULL};
    execv("/bin/unzip", argv1); 
    }
}
```

Pada fungsi main dilakukan forking, pada child process pertama dilakukan download menggunakan argumen wget dan syntax execv berdasarkan link yang telah diberikan Bersamaan dengan pembuatan folder ./hehe/ untuk directory unzip file yang telah didownlaod. Pada parent process dilakukan unzip file yang telah didownload ke ./hehe/ menggunakan argumen unzip dan execv.

## categorize.c 
```
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <wait.h>
#include <string.h>
#include <time.h>
#include <dirent.h>
#include <pthread.h>
#include <ctype.h>

char absoluteHehe[100];
char absoluteCategorized[100];
char absoluteLog[100];
char absoluteMax[100];
int maxFile;
```

Pertama include library dan variabel global yang akan digunakan. absoluteHehe adalah path ./hehe/files/, absoluteCategorized adalah path ./categorized/, absolute log adalah path ./log.txt, absoluteMax adalah path ./hehe/max.txt, dan maxFile adalah jumlah file maximum pada 1 folder.

```
//0: emc; 1: jpg; 2: js; 3: png; 4: py; 5: txt; 6: xyz; 7: other
int foldIdx[9]= {0, 0, 0, 0, 0, 0, 0};
int numFile[9]= {0, 0, 0, 0, 0, 0, 0};
```

Selanjutnya inisiasi array foldIdx yg merupakan array indexing jenis extension folder dan array numFile yg merupakan array counter jumlah file pada sebuah extension.

```
void logCreate(char *dirName){
    FILE *log = fopen(absoluteLog, "a");

    time_t rawtime;
    char times[150];

    time(&rawtime);
    strftime(times, 80, "%d-%m-%Y %H:%M:%S", localtime(&rawtime));
    strcat(times, " MADE ");
    strcat(times, dirName);
    fprintf(log, "%s\n", times);
    fclose(log);
}
```

Terdapat fungsi logCreate yang berfungsi untuk membuat log message membuat folder ke ./log.txt

```
void logCreateAcc(char *Args){
    FILE *log = fopen(absoluteLog, "a");

    time_t rawtime;
    char times[1001];

    time(&rawtime);
    strftime(times, 80, "%d-%m-%Y %H:%M:%S", localtime(&rawtime));
    strcat(times, " ACCESSED ");
    strcat(times, Args);
    fprintf(log, "%s\n", times);
    fclose(log);
}
```

Terdapat fungsi logCreateAcc yang berfungsi untuk membuat log message akses folder ke ./log.txt

```
void logCreateMov(char *src, char *dest){
    FILE *log = fopen(absoluteLog, "a");

    time_t rawtime;
    char times[1001];

    time(&rawtime);
    strftime(times, 80, "%d-%m-%Y %H:%M:%S", localtime(&rawtime));
    strcat(times, " MOVE ");
    strcat(times, src);
    strcat(times, " > ");
    strcat(times, dest);
    fprintf(log, "%s\n", times);
    fclose(log);
}
```

Terdapat fungsi logCreateMov yang berfungsi untuk membuat log message move file dari src menuju dest ke ./log.txt

```
void cek_jumlahFile_folder(char *folderext, char *type, int idx){
	DIR *dir2;
    struct dirent *ent;
    int count = 0;

    printf("move to: %s\n", folderext);
    dir2 = opendir(folderext);
    if (dir2 == NULL) {
        perror("opendir");
        pthread_exit(NULL);
	}
	else{
        while ((ent = readdir(dir2)) != NULL) {
            if (ent->d_type == DT_REG){
                count++;
            }
        }
        if(count > maxFile){
            foldIdx[idx]++;
            char newFold[20];
            sprintf(newFold, "%s %d", type, foldIdx[idx]);
            mkdir(newFold, 0700);
            logCreate(newFold);
        }
        closedir(dir2);
        printf("success\n");
    }
}
```

Fungsi cek_jumlahFile_folder merupakan fungsi evaluasi jumlah file pada index folder tertentu, jika jumlah file pada folder berjumlah 10 maka increment index pada tipe extension tersebut dan buat folder baru menggunakan index terbaru. dan buat log Create untuk menuliskan ke log.

```
void move_file(char *src, char *filename){
    char dst[1001] = "";
    strcpy(dst, absoluteCategorized);
    char *extension = strrchr(filename, '.');

    char dstNonFile[2001];
    
    char foldDest[101];
    if(extension == NULL){
        // file "jangan dibuka" extension nya null tapi file
        // move ke other
        printf(" is a null other file\n");

        sprintf(foldDest, "other %d", foldIdx[7]);
        strcat(dst, foldDest);
        strcpy(dstNonFile, dst);
        strcat(dst, "/");
        strcat(dst, filename);

        rename(src, dst);
        cek_jumlahFile_folder(dstNonFile, "other", 7);
        logCreateMov(src, dst);
        numFile[7]++;
    }
    else{
        for(int i=0; i< strlen(extension); i++){
            extension[i] = tolower(extension[i]);
    	}
    	//printf("%s\n", filename);
    	char command[5001];
        //cek extension
        if(extension != NULL && strcmp(extension+1, "emc")==0){
        	// move ke folder emc
            printf(" is a emc file\n");

            sprintf(foldDest, "emc %d", foldIdx[0]);
            strcat(dst, foldDest);
            strcpy(dstNonFile, dst);
            strcat(dst, "/");
            strcat(dst, filename);

        	rename(src, dst);
            cek_jumlahFile_folder(dstNonFile, "emc", 0);
            logCreateMov(src, dst);
            numFile[0]++;		    
        }
        else if(extension != NULL && strcmp(extension+1, "jpg")==0){
            // move ke folder jpg
            printf(" is a jpg file\n");

            sprintf(foldDest, "jpg %d", foldIdx[1]);
            strcat(dst, foldDest);
            strcpy(dstNonFile, dst);
            strcat(dst, "/");
            strcat(dst, filename);

        	rename(src, dst);
            cek_jumlahFile_folder(dstNonFile, "jpg", 1);
            logCreateMov(src, dst);
            numFile[1]++;
        }
        else if(extension != NULL && strcmp(extension+1, "js")==0){
            // move ke folder js
            printf(" is a js file\n");

            sprintf(foldDest, "js %d", foldIdx[2]);
            strcat(dst, foldDest);
            strcpy(dstNonFile, dst);
            strcat(dst, "/");
            strcat(dst, filename);

        	rename(src, dst);
            cek_jumlahFile_folder(dstNonFile, "js", 2);
            logCreateMov(src, dst);
            numFile[2]++;
        }
        else if(extension != NULL && strcmp(extension+1, "png")==0){
            // move ke folder png
            printf(" is a png file\n");

            sprintf(foldDest, "png %d", foldIdx[3]);
            strcat(dst, foldDest);
            strcpy(dstNonFile, dst);
            strcat(dst, "/");
            strcat(dst, filename);

        	rename(src, dst);
            cek_jumlahFile_folder(dstNonFile, "png", 3);
            logCreateMov(src, dst);
            numFile[3]++;
        }
        else if(extension != NULL && strcmp(extension+1, "py")==0){
            // move ke folder py
            printf(" is a py file\n");

            sprintf(foldDest, "py %d", foldIdx[4]);
            strcat(dst, foldDest);
            strcpy(dstNonFile, dst);
            strcat(dst, "/");
            strcat(dst, filename);

        	rename(src, dst);
            cek_jumlahFile_folder(dstNonFile, "py", 4);
            logCreateMov(src, dst);
            numFile[4]++;
        }
        else if(extension != NULL && strcmp(extension+1, "txt")==0){
            // move ke folder txt
            printf(" is a txt file\n");

            sprintf(foldDest, "txt %d", foldIdx[5]);
            strcat(dst, foldDest);
            strcpy(dstNonFile, dst);
            strcat(dst, "/");
            strcat(dst, filename);

        	rename(src, dst);
            cek_jumlahFile_folder(dstNonFile, "txt", 5);
            logCreateMov(src, dst);
            numFile[5]++;
        }
        else if(extension != NULL && strcmp(extension+1, "xyz")==0){
            // move ke folder xyz
            printf(" is a xyz file\n");

            sprintf(foldDest, "xyz %d", foldIdx[6]);
            strcat(dst, foldDest);
            strcpy(dstNonFile, dst);
            strcat(dst, "/");
            strcat(dst, filename);

        	rename(src, dst);
            cek_jumlahFile_folder(dstNonFile, "xyz", 6);
            logCreateMov(src, dst);
            numFile[6]++;
        }
        else{
            // move ke folder other
            printf(" is a non-Null other file\n");

            sprintf(foldDest, "other %d", foldIdx[7]);
            strcat(dst, foldDest);
            strcpy(dstNonFile, dst);
            strcat(dst, "/");
            strcat(dst, filename);

            rename(src, dst);
            cek_jumlahFile_folder(dstNonFile, "other", 7);
            logCreateMov(src, dst);
            numFile[7]++;  
        }
    }
    printf("\n");

}
```

Selanjutnya terdapat fungsi move_file yang digunakan untuk klasifikasi jenis extension file yang diterima via parameter. Pada fungsi move_file ini akan diextract extension dari filename menggunakan strrchr dengan delimiter '.'. Setelah ditemukan jenis extensionnya maka akan direname menuju folder yang sesuai dengan extensionnya dan berdasarkan index terkini untuk extension tersebut. Juga dipanggil fungsi cek_jumlahFile_folder untuk evaluasi indexing folder serta fungsi logCreateMov untuk membuat log move.

```
void *thread_dir_list(void *arg) {
    char *path = (char *) arg;
    DIR *dir;
    struct dirent *entry;

    // Open directory
    dir = opendir(path);
    if (dir == NULL) {
        perror("opendir");
        pthread_exit(NULL);
    }

    // Loop over directory entries
    while ((entry = readdir(dir)) != NULL) {
        char fullpath[1001];

        if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) {
            // Skip current and parent directory entries
            continue;
        }

        // Print entry name
        printf("%s/%s\n", path, entry->d_name);

        if (entry->d_type == DT_DIR) {
            // Entry is a directory, create new thread to list it
			logCreateAcc(entry->d_name);
            pthread_t tid;
            sprintf(fullpath, "%s/%s", path, entry->d_name);

            if (pthread_create(&tid, NULL, thread_dir_list, fullpath) != 0) {
                perror("pthread_create");
                continue;
            }

            // Wait for thread to finish
            if (pthread_join(tid, NULL) != 0) {
                perror("pthread_join");
                continue;
            }
        } else {
            // Entry is a file
            char filePath[1001];
            strcpy(filePath, path);
            strcat(filePath, "/");
            strcat(filePath, entry->d_name);

            move_file(filePath, entry->d_name);
        }
    }

    closedir(dir);
    pthread_exit(NULL);
}
```

Selanjutnya terdapat fungsi thread_dir_list untuk directory listing menggunakan thread. thread_dir_list merupakan fungsi rekursif directory listing pada umumnya namun menggunakan multithreading pada rekursifnya. Pada fungsi tersebut akan dilihat apakah entry merupakan directory atau file. Jika entry merupakan directory maka buat thread untuk directory listing pada directory tersebut, namun jika entry merupakan file maka pindah file tersebut berdasarkan klasifikasi pada fungsi move_file.

```
int getMax(){
    int res;
    FILE *max = fopen(absoluteMax, "r");
    fscanf(max, "%d", &res);
    fclose(max);
    return res;
}
```

Juga terdapat fungsi getMax untuk membaca integer pada ./hehe/max.txt.

```
int main(){
    char cwd[101];
    getcwd(cwd, sizeof(cwd));

    strcpy(absoluteLog, cwd);
    strcat(absoluteLog, "/log.txt");
    strcpy(absoluteMax, cwd);
    strcat(absoluteMax, "/hehe/max.txt");
    strcpy(absoluteHehe, cwd);
    strcat(absoluteHehe, "/hehe/files");
    strcpy(absoluteCategorized, cwd);
    strcat(absoluteCategorized, "/categorized/");

    strcat(cwd, "/hehe/extensions.txt");

    maxFile = getMax()-1;

    FILE *ext;
    ext = fopen(cwd, "r");

    mkdir("categorized", 0700);
    logCreate("categorized");
    
    chdir("categorized");
    char dirname[10];
    while(fscanf(ext, "%s", dirname) == 1){
        strcat(dirname, " 0");
        mkdir(dirname, 0700);
        logCreate(dirname);
    }
    mkdir("other 0", 0700);
    logCreate("other 0");

    pthread_t tid;
	logCreateAcc("files");
    if (pthread_create(&tid, NULL, thread_dir_list, absoluteHehe) != 0) {
        perror("pthread_create");
        exit(EXIT_FAILURE);
    }
    // Wait for thread to finish
    if (pthread_join(tid, NULL) != 0) {
        perror("pthread_join");
        exit(EXIT_FAILURE);
    }
    printf("emc: %d\njpg: %d\njs: %d\npng: %d\npy: %d\ntxt: %d\nxyz: %d\nother: %d\n", numFile[0],numFile[1],numFile[2],numFile[3],numFile[4],numFile[5],numFile[6],numFile[7]);

    return 0;
}
```

Pada fungsi main tahap pertama adalah mendapat current work directory untuk dijadikan absolute path-path yang akan digunakan nanti (absoluteLog, absoluteMax, absoluteHehe, dan absoluteCategorized). Setelah itu setting global variable maxFile sesuai getMax()-1. setelah itu buka file ./hehe/extensions.txt dilanjutkan dengan mkdir folder "categorized" dan logCreate nya. Lalu change directory ke folder categorized tersebut untuk dilakukan pembuatan folder berdasarkan extensionnya dimulai dari index 0, termasuk folder "other" beserta dengan logCreate nya. Setelah itu directory listing folder ./hehe/files/ menggunakan thread dan pemanggilan fungsi thread thread_dir_list. Jika semua thread telah finish maka akan dibandingkan jumlah files pada masing-masing extension.


## logchecker.c
```
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

struct Pair
{
    char *str;
    int num;
};

int main()
{
    const char *filePath = "log.txt";
    FILE *file = fopen(filePath, "r");

    char line[2051];
    int jml_acc = 0;

    // hitung ACCESSED
    while (fgets(line, sizeof(line), file) != NULL)
    {
        if (strstr(line, "ACCESSED") != NULL)
        {
            jml_acc++;
        }
    }

    printf("jumlah ACCESSED: %d\n", jml_acc);
    fseek(file, 0, SEEK_SET);
```
Pertama buka file log.txt 
Lalu baca semua line dalam file log menggunakan fgets setelah itu cek line mana yang ada kata ACCESSED nya lalu hitung jumlahnya dan print. 
![image](https://github.com/Rafi-Sutrisno/Tugas-PBO/blob/main/JumlahAcc.png?raw=true)
```
    printf("\nTOTAL PER FOLDER urut Ascending\n");
    char **folders = NULL;
    int folder_count = 0;
    while (fgets(line, sizeof(line), file) != NULL)
    {
        if (strstr(line, "MADE") != NULL)
        {
            char *folder_name = line;
            int spaceCount = 0;
            while (*folder_name != '\0')
            {
                if (*folder_name == ' ')
                {
                    spaceCount++;
                    if (spaceCount == 3)
                    {
                        folder_name++;
                        break;
                    }
                }
                folder_name++;
            }

            char *end = strchr(folder_name, '\n');
            if (end != NULL)
            {
                *end = '\0';
            }

            folders = realloc(folders, (folder_count + 1) * sizeof(char *));
            folders[folder_count] = strdup(folder_name);
            folder_count++;
        }
    }
```
Setelah itu baca lagi line dalam file log dan cek line yang mengandung kata MADE maka artinya itu adalah membuat directory, lalu simpan nama directory tersebut.
```
    int total[folder_count];
    int arcount[folder_count];
    for (int i = 0; i < folder_count; i++)
    {

        fseek(file, 0, SEEK_SET);
        int count = 0;

        while (fgets(line, sizeof(line), file) != NULL)
        {
            if (strstr(line, "MOVE") != NULL && strstr(line, folders[i]) != NULL)
            {
                count++;
            }
        }
		arcount[i] = count;
    }
```
Setelah berhasil menyimpan semua nama directory selanjutya cek setiap nama direcotry lalu baca file log dan cek line yang ada kata MOVE dan nama directory nya. lalu hitung jumlahnya. Maka artinya kita dapat tahu directory tersebut memeliki berapa file.
```
    for(int i = 0; i <= 10; i++){
    	for(int j = 0; j < folder_count; j++){
    		if(arcount[j] == i){
    			printf("%s: %d\n", folders[j], arcount[j]);
			}
		}
	}
```
Setelah itu print secara ascending dari terkecil.
![image](https://github.com/Rafi-Sutrisno/Tugas-PBO/blob/main/jumlahfile.png?raw=true)
```
	char arexten[7][4] = {"emc", "jpg", "js", "png", "py", "txt", "xyz"};
	int arex[7];
	while (fgets(line, sizeof(line), file) != NULL)
    {
        if (strstr(line, "MOVE") != NULL && strstr(line, "/emc") != NULL && strstr(line, "/other") == NULL)
        {
            arex[0]++;
        }else if (strstr(line, "MOVE") != NULL && strstr(line, "/jpg") != NULL && strstr(line, "/other") == NULL)
        {
            arex[1]++;
        }else if (strstr(line, "MOVE") != NULL && strstr(line, "/js") != NULL && strstr(line, "/other") == NULL)
        {
            arex[2]++;
        }else if (strstr(line, "MOVE") != NULL && strstr(line, "/png") != NULL && strstr(line, "/other") == NULL)
        {
            arex[3]++;
        }else if (strstr(line, "MOVE") != NULL && strstr(line, "/py") != NULL && strstr(line, "/other") == NULL)
        {
            arex[4]++;
        }else if (strstr(line, "MOVE") != NULL && strstr(line, "/txt") != NULL && strstr(line, "/other") == NULL)
        {
            arex[5]++;
        }else if (strstr(line, "MOVE") != NULL && strstr(line, "/xyz") != NULL && strstr(line, "/other") == NULL)
        {
            arex[6]++;
        }
    }
```
Setelah itu cek masing masing extension dan jumlah file nya.
```
    printf("\n\n");
	for(int i = 0; i < 75; i++){
		for(int j = 0; j < 7; j++){
			if(arex[j] == i){
				printf("%s : %d\n", arexten[j], arex[j]);
			}
		}
	}
```
Setelah itu print secara ascending.
```
    fclose(file);
    return 0;
}

```
