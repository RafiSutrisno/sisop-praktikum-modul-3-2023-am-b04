// C program to demonstrate use of fork() and pipe() 
#include<stdio.h> 
#include<stdlib.h> 
#include<unistd.h> 
#include<sys/types.h> 
#include<string.h> 
#include<sys/wait.h> 
#include<ctype.h>

#define MAX_TREE_HT 100

typedef struct{
    int msglen;
    char msg[1001];
    int regCharCount;
    char regChar[25];
    int charFreq[25];
} msgSend;

typedef struct{
    int msglen;
    char msg[1001][10];
    int regCharCount;
    char regChar[25];
    char regCharCode[25][10];
} msgBack;

struct NodeHuffman {
    char data;
    unsigned freq;
    struct NodeHuffman *left, *right;
};

struct MinHeap {
    unsigned size;
    unsigned kapasitas;
 
    struct NodeHuffman** array;
};

struct NodeHuffman* buat_Node(char data, unsigned freq){
    struct NodeHuffman* newNode = (struct NodeHuffman*)malloc(sizeof(struct NodeHuffman));
 
    newNode->left = newNode->right = NULL;
    
    newNode->freq = freq;
    newNode->data = data;
 
    return newNode;
}

struct MinHeap* create_huffmanMinHeap(unsigned kapasitas){
 
    struct MinHeap* MinHeap = (struct MinHeap*)malloc(sizeof(struct MinHeap));
 
    MinHeap->size = 0;
    MinHeap->kapasitas = kapasitas;
 
    MinHeap->array = (struct NodeHuffman**)malloc(MinHeap->kapasitas * sizeof(struct NodeHuffman*));
    return MinHeap;
}

void tukarNode(struct NodeHuffman** a, struct NodeHuffman** b){
 
    struct NodeHuffman* t = *a;
    *a = *b;
    *b = t;
}

void restruktur_MinHeap(struct MinHeap* MinHeap, int idx){
    int cur_small = idx;
    int left = 2 * idx + 1;
    int right = 2 * idx + 2;
 
    if (left < MinHeap->size && MinHeap->array[left]->freq < MinHeap->array[cur_small]->freq)
        cur_small = left;
 
    if (right < MinHeap->size && MinHeap->array[right]->freq < MinHeap->array[cur_small]->freq)
        cur_small = right;
 
    if (cur_small != idx) {
        tukarNode(&MinHeap->array[cur_small], &MinHeap->array[idx]);
        restruktur_MinHeap(MinHeap, cur_small);
    }
}

int isSizeOne(struct MinHeap* MinHeap){
    return (MinHeap->size == 1);
}
 
struct NodeHuffman* extractMin(struct MinHeap* MinHeap){
 
    struct NodeHuffman* temp = MinHeap->array[0];
    MinHeap->array[0] = MinHeap->array[MinHeap->size - 1];
 
    --MinHeap->size;
    restruktur_MinHeap(MinHeap, 0);
 
    return temp;
}

void insertToHeap(struct MinHeap* MinHeap, struct NodeHuffman* NodeHuffman){
 
    ++MinHeap->size;
    int i = MinHeap->size - 1;
 
    while (NodeHuffman->freq < MinHeap->array[(i - 1) / 2]->freq && i) {

        MinHeap->array[i] = MinHeap->array[(i - 1) / 2];
        i = (i - 1) / 2;
    }
 
    MinHeap->array[i] = NodeHuffman;
}

void initMinHeap(struct MinHeap* MinHeap){
    int n = MinHeap->size - 1;
    int i;
 
    for (i = (n - 1) / 2; i >= 0; --i)
        restruktur_MinHeap(MinHeap, i);
}
 
void printCodeArr(int arr[], int n){
    for (int i = 0; i < n; ++i)
        printf("%d", arr[i]);
 
    printf("\n");
}

int isLetterNode(struct NodeHuffman* root){
    return !(root->left) && !(root->right);
}

struct MinHeap* createAndinitMinHeap(char data[], int freq[], int size){
 
    struct MinHeap* MinHeap = create_huffmanMinHeap(size);
 
    for (int i = 0; i < size; ++i)
        MinHeap->array[i] = buat_Node(data[i], freq[i]);
 
    MinHeap->size = size;
    initMinHeap(MinHeap);
 
    return MinHeap;
}
 

struct NodeHuffman* buildHuffmanTree(char data[],
                                     int freq[], int size){
    struct NodeHuffman *left, *right, *top;
    struct MinHeap* MinHeap
        = createAndinitMinHeap(data, freq, size);

    while (!isSizeOne(MinHeap)) {
        left = extractMin(MinHeap);
        right = extractMin(MinHeap);
        top = buat_Node('$', left->freq + right->freq);
 
        top->left = left;
        top->right = right;
 
        insertToHeap(MinHeap, top);
    }
    return extractMin(MinHeap);
}

int assign = 0;
void assignCodes(struct NodeHuffman* root, char arr[], int top, msgBack *data){

    if (root->left) {
        arr[top] = '0';
        assignCodes(root->left, arr, top + 1, data);
    }

    if (root->right) {
        arr[top] = '1';
        assignCodes(root->right, arr, top + 1, data);
    }

    if (isLetterNode(root)) {
        data->regChar[assign] = root->data;
        strcpy(data->regCharCode[assign], arr);

        data->regCharCode[assign][top] = '\0';
        assign++;
    }
}

struct NodeHuffman* HuffmanCodes(char data[], int freq[], int size){
    struct NodeHuffman* root
        = buildHuffmanTree(data, freq, size);
    
    return root;
}

int main() { 

	int fd1[2];
	int fd2[2];
	pid_t p; 

	if (pipe(fd1)==-1) { 
		fprintf(stderr, "Pipe Failed" ); 
		return 1; 
	} 
	if (pipe(fd2)==-1) { 
		fprintf(stderr, "Pipe Failed" ); 
		return 1; 
	} 

	p = fork(); 

	if (p < 0) { 
		fprintf(stderr, "fork Failed" ); 
		return 1; 
	} 

	// Parent process 
	else if (p > 0) { 

		close(fd1[0]); // Close reading end of first pipe 

        /////////////////// count sect //////////////////////////////

        msgSend data;

        char res[1001];
        int  i,j,k,count=0,n;
        char ch;

        FILE  *fpin;
        fpin = fopen("file.txt", "r");
        int tl=0;
        while((ch=getc(fpin))!=EOF){
            char buf[2];
            if((ch>='a' && ch<='z') | (ch>='A' && ch<='Z')){
                buf[0] = toupper(ch);
                strcat(res, buf);
                tl++;
            }
            else if(ch == ' '){
                strcat(res, " ");
                tl++;
            }
        }
        data.msglen = strlen(res);
        strcpy(data.msg, res);

        fclose(fpin);

        long len = strlen(res);
        int regCharCount = -1;
        for(i=0;i<len;i++){
            count=1;
            if(res[i] == '\0'){continue;}

            // jika distinct char
            regCharCount++;
            for(j=i+1;j<len;j++) {   
                if(res[i]==res[j]) {
                    count++;
                    res[j]='\0';
                }
            }
            data.regChar[regCharCount] = res[i];
            data.charFreq[regCharCount] = count;
        }
        data.regCharCount = regCharCount+1;

        if(write(fd1[1], &data, sizeof(msgSend))<0){printf("error2\n");return 2;}
        msgBack data4;
        /////////////////// count sect //////////////////////////////

		close(fd1[1]);

		wait(NULL);

		close(fd2[1]);

        ////////////////// recieve back sect //////////////////////////

        if(read(fd2[0], &data4, sizeof(msgBack))<0){printf("error5\n");return 5;}

        ////////////////// recieve back sect //////////////////////////

        int bitCount = 0;
        for(int i=0; i<data4.msglen; i++){
            bitCount += strlen(data4.msg[i]);
        }

        printf("\nencryption:\n");
        for(int i=0; i<data4.msglen; i++){
            printf("%s ", data4.msg[i]);
        }
        printf("\n\nDecryption:\n");

        for(int i=0; i<data4.msglen; i++){
            for(int j=0; j<24; j++){
                if(!strcmp(data4.msg[i], data4.regCharCode[j])){
                    printf("%c", data4.regChar[j]);
                }
            }
        }
        printf("\n\n");

        printf("total huruf: %d\nhuffman bit: %d\nASCII bit: %d\n", data4.msglen, bitCount, data4.msglen*8);

		close(fd2[0]); 
	} 

	// child process 
	else{ 
		close(fd1[1]); // Close writing end of first pipe 
        
        /////////////////// recieve child sect ///////////////////////
		
        msgSend data2;
        msgBack data3;

        if(read(fd1[0], &data2, sizeof(msgSend))<0){printf("error3\n");return 3;}

        struct NodeHuffman* bufroot = HuffmanCodes(data2.regChar, data2.charFreq , data2.regCharCount);
        
        char arr[MAX_TREE_HT];
        int top = 0;
        assignCodes(bufroot, arr, top, &data3);

        data3.msglen = data2.msglen;

        for(int i=0; i<data2.msglen; i++){
            for(int j=0; j<24; j++){
                if(data2.msg[i] == data3.regChar[j]){
                    strcpy(data3.msg[i], data3.regCharCode[j]);
                }
            }
        }

		/////////////////// recieve child sect ///////////////////////
		close(fd1[0]); 
		close(fd2[0]); 

		/////////////////// send back sect ////////////////////////////
        if(write(fd2[1], &data3, sizeof(msgBack))<0){printf("error4\n");return 4;}
		/////////////////// send back sect ////////////////////////////

		close(fd2[1]); 

		exit(0); 
	} 
} 