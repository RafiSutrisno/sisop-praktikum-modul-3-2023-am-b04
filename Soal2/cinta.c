#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>
#include <pthread.h>
#include <time.h>

#define ROWS 4
#define COLS 5
#define SHM_SIZE (sizeof(int) * ROWS * COLS)

int array[ROWS*COLS];
int result[ROWS*COLS];

void *faktorial(void *no){
        int *k_num = (int *)(no);
        long long factor = 1;
        for(int i = array[*k_num]; i > 0; i--){
            factor *= i;
        }
        result[*k_num] = factor;
        
        long k = 1000000000;
        while(k--){}
}


int main(){

        key_t randomkey = 123456;
        int shm_id = shmget(randomkey, SHM_SIZE, 0666|IPC_CREAT);

        int *shm = (int *) shmat(shm_id, NULL, 0);
        if (shm == (int *) -1) {
            perror("shmat");
            exit(1);
        }
		
        int (*shared_array)[COLS] = (int (*)[COLS]) shm;
		int k = 0;
        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < COLS; j++) {
                array[k] = shared_array[i][j];
                k++;
            }
        }
        shmdt(shared_array);
        shmctl(shm_id, IPC_RMID, NULL);
		
		int a=0;
        printf("\nHasil Perkalian Matriks : \n");
        for(int i = 0; i < 20; i++){               
            printf("%d ", array[i]);
            a++;
			if(a==5){
               printf("\n");
               a = 0;
        	}
        }
        printf("\n");

        int jumlah = ROWS*COLS;
        pthread_t thread[jumlah];
        
        int no[jumlah];
        for(int i = 0; i < 20; i++){
        	no[i]=i;
		}
        
        for(int i = 0; i < jumlah; i++){
            pthread_create(&thread[i], NULL, faktorial, (void*) &no[i]);
        }
        for(int k = 0; k < jumlah; k++){
            pthread_join(thread[k], NULL);
        }
		
		a = 0;
		printf("\nHasil Faktorial Matriks : \n");
        for(int i = 0; i < 20; i++){               
            printf("%d ", result[i]);
            a++;
			if(a==5){
               printf("\n");
               a = 0;
        	}
        }
        return 0;
}

