#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>


#define ROWS 4
#define COLS 5
#define SHM_SIZE (sizeof(int) * ROWS * COLS)

int main(){
	
	int array1[4][2], array2[2][5], hasil[4][5];
	srand(time(0));
	for(int i = 0; i < 4; i++){
		for(int j = 0; j < 2; j++){
			array1[i][j] = (rand() % (5 - 1 + 1)) + 1;
		}
	}
	for(int i = 0; i < 2; i++){
		for(int j = 0; j < 5; j++){
			array2[i][j] = (rand() % (4 - 1 + 1)) + 1;
		}
	}
	
	for(int i = 0; i < 4; i++){
		for(int j = 0; j < 5; j++){
			hasil[i][j] = 0;
			for(int k = 0; k < 2; k++){
				hasil[i][j] += array1[i][k] * array2[k][j];
			}
		}
	}
	
	for(int i = 0; i < 4; i++){
		for(int j = 0; j < 2; j++){
			printf("%d ", array1[i][j]);
		}
		printf("\n");
	}
	printf("\n");
	for(int i = 0; i < 2; i++){
		for(int j = 0; j < 5; j++){
			printf("%d ", array2[i][j]);
		}
		printf("\n");
	}
	printf("\nHasil Matriks : \n");
	for(int i = 0; i < 4; i++){
		for(int j = 0; j < 5; j++){
			printf("%d ", hasil[i][j]);
		}
		printf("\n");
	}
	
	key_t randomkey = 123456;
	int shm_id = shmget(randomkey, SHM_SIZE, 0666|IPC_CREAT);
	
	int *shm = (int *) shmat(shm_id, NULL, 0);
	if (shm == (int *) -1) {
	    perror("shmat");
	    exit(1);
	}

	int (*shared_array)[COLS] = (int (*)[COLS]) shm;
	
	key_t randomkey2 = 1234567;
	int shm_id2 = shmget(randomkey2, SHM_SIZE, 0666|IPC_CREAT);
	
	int *shm2 = (int *) shmat(shm_id2, NULL, 0);
	if (shm2 == (int *) -1) {
	    perror("shmat");
	    exit(1);
	}

	int (*shared_array2)[COLS] = (int (*)[COLS]) shm2;
	
	for (int i = 0; i < ROWS; i++) {
	    for (int j = 0; j < COLS; j++) {
	    	shared_array[i][j] = hasil[i][j];
	        shared_array2[i][j] = hasil[i][j];
	    }
	}
	
	sleep(10);
	shmdt(shared_array);
	shmctl(shm_id, IPC_RMID, NULL);
	shmdt(shared_array2);
	shmctl(shm_id2, IPC_RMID, NULL);
	
	return 0;
}



