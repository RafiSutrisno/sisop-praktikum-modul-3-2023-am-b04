#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>
#include <time.h>


#define ROWS 4
#define COLS 5
#define SHM_SIZE (sizeof(int) * ROWS * COLS)

int faktorial(int var){
        long long factor = 1;
        for(int i = var; i > 0; i--){
            factor *= i;
        }
        long k = 1000000000;
        while(k--){}
        return factor;
}

int main(){

        int array[ROWS][COLS];

        key_t randomkey = 1234567;
        int shm_id = shmget(randomkey, SHM_SIZE, 0666|IPC_CREAT);

        int *shm = (int *) shmat(shm_id, NULL, 0);
        if (shm == (int *) -1) {
            perror("shmat");
            exit(1);
        }

        int (*shared_array)[COLS] = (int (*)[COLS]) shm;

        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < COLS; j++) {
                array[i][j] = shared_array[i][j];
            }
        }
        shmdt(shared_array);
        shmctl(shm_id, IPC_RMID, NULL);

        printf("\nHasil Perkalian Matriks : \n");
        for(int i = 0; i < 4; i++){
                for(int j = 0; j < 5; j++){
                        printf("%d ", array[i][j]);
                }
                printf("\n");
        }
        printf("\n");

        long long factor;
        int num;
        for(int i = 0; i < 4; i++){
                for(int j = 0; j < 5; j++){
                        array[i][j] = faktorial(array[i][j]);
                }
        }
		
		printf("\nHasil Faktorial Matriks : \n");
        for(int i = 0; i < 4; i++){
                for(int j = 0; j < 5; j++){
                        printf("%d ", array[i][j]);
                }
                printf("\n");
        }
        printf("\n");
        
        return 0;
}

