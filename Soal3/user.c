// C Program for Message Queue (Writer Process)
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <semaphore.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/msg.h>

#define MAX 1024
  
// structure for message queue
struct mesg_buffer {
    long mesg_type;
    int user_pid;
    char mesg_text[1024];
} message;
  
int main(){
    key_t key;
    int msgid;
  
    key = ftok("stream_playlist", 50); // ftok membuat key unique
  
    msgid = msgget(key, 0666 | IPC_CREAT); // msgget membuat message queue dan identifier
    message.mesg_type = 1;
    message.user_pid = getpid();

    sem_t *sem_stream = sem_open("/stream", 0); // Untuk mengakses semaphore sem_stream
    if(sem_stream == SEM_FAILED){
        perror("sem_open/stream");
        exit(EXIT_FAILURE);
    }

    sem_t *sem_user = sem_open("/user", 0); // Untuk mengakses semaphore sem_user
    if(sem_user == SEM_FAILED){
        perror("sem_open/user");
        exit(EXIT_FAILURE);
    }
    
    int sem_val;
    sem_getvalue(sem_user, &sem_val);

    if(sem_val == 0){ // Jika semaphore sem_user penuh, maka tidak diperbolehkan masuk
        printf("STREAM SYSTEM OVERLOAD\n");
        return 1;
    }

    while(1){
        printf("Waiting...\n");

        sem_wait(sem_user);

        printf("Insert Data:");
        fgets(message.mesg_text,MAX,stdin); // Menginput pesan
        
        if(strcmp("EXIT\n", message.mesg_text) == 0){ // Jika pesan EXIT maka keluar program user
            sem_post(sem_user);
            return 0;
        }
        
        msgsnd(msgid, &message, sizeof(message), 0); // Mengirim melalui message queue
        
        printf("Data sent: %s \n", message.mesg_text); // Display 

        sem_post(sem_stream); // Sinyal stream untuk memproses message
    }
  
    return 0;
}