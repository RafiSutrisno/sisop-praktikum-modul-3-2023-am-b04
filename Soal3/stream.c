#define _GNU_SOURCE

#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <json-c/json.h>
#include <semaphore.h>
#include <regex.h>
#include <sys/ipc.h>
#include <sys/msg.h>

// structure for message queue
struct mesg_buffer {
    long mesg_type;
    int user_pid;
    char mesg_text[1024];
} message;

const char base64_map[] =   {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                            'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                            'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                            'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'};

int countinplaylist(char *fname, char *str){ // Fungsi untuk menghitung banyak line yang mengandung input di playlist.txt
    FILE *fp;
    int find_count = 0;
    char temp[1024];

    if((fp = fopen(fname, "r")) == NULL){ // Membuka playlist.txt
        return(-1);
    }

    while(fgets(temp, sizeof(temp), fp) != NULL){
        if((strlen(temp) > 0) && (temp[strlen(temp) - 1] == '\n')) temp[strlen(temp) - 1] = '\0'; // Membandingkan apakah di line terdapat input (case insensitive)
        if((strcasestr(temp, str)) != NULL) find_count++; // Increment jika menemukan
    }

    if(fp){
        fclose(fp);
    }

    return find_count;
}

int findinplaylist(char *fname, char *str){ // Fungsi untuk mencari secara eksak sesuai input
    FILE *fp;
    int find_count = 0;
    char temp[1024];

    if((fp = fopen(fname, "r")) == NULL){ // Membuka playlist.txt
        return(-1);
    }

    while(fgets(temp, sizeof(temp), fp) != NULL){ // Membandingkan dengan input
        if((strlen(temp) > 0) && (temp[strlen(temp) - 1] == '\n')) temp[strlen(temp) - 1] = '\0';
        if((!strcasecmp(temp, str))) find_count++; // Membandingkan apakah line sama dengan input
    }

    if(fp){
        fclose(fp);
    }

    return find_count;
}

void base64_decode(char* str) { // Fungsi untuk decode base64
    char counts = 0;
    char buffer[4];
    char* plain = malloc(strlen(str) * 3 / 4 + 1);
    int i = 0, p = 0;

    for(i = 0; str[i] != '\0'; i++) {
        char k;
        for(k = 0 ; k < 64 && base64_map[k] != str[i]; k++);
        buffer[counts++] = k;
        if(counts == 4) {
            plain[p++] = (buffer[0] << 2) + (buffer[1] >> 4);
            if(buffer[2] != 64) plain[p++] = (buffer[1] << 4) + (buffer[2] >> 2);
            if(buffer[3] != 64) plain[p++] = (buffer[2] << 6) + buffer[3];
            counts = 0;
        }
    }

    plain[p] = '\0';   
    strcpy(str, plain);
}

void rot13_decode(char *str){
    char c;
    while(*str){ // Fungsi untuk decode rot13
        c = *str;
        if(isalpha(c)){ // Untuk a hingga m ditambah 13 karakter
            if(c >= 'a' && c <= 'm' || c >= 'A' && c <= 'M')*str += 13;
            else *str -= 13; // Selain itu, dikurang 13
        } str++;
    }
}

void hex_decode(char *str){
    char string[1024];
    int len = strlen(str);
    for(int i = 0, j = 0; j < len; ++i, j += 2){ // Fungsi untuk decode hex
        int val[1];
        sscanf(str + j, "%2x", val);
        string[i] = val[0];
        string[i + 1] = '\0';
    }
    strcpy(str, string);
}

void sort(){
    char command[50];
    strcpy(command, "sort -u playlist.txt -o playlist.txt"); // -u menghapus duplikat
    system(command);
}

void decrypt(){
    const char *fp = "song-playlist.json";

    struct json_object *parsed_json;
    
    parsed_json = json_object_from_file(fp); // Mengambil file song-playlist.json

    if(!json_object_is_type(parsed_json, json_type_array)){ // Mengecek apakah json berupa array
        printf("Array not Found!\n");
        return;
    }

    int array_len = json_object_array_length(parsed_json);

    for(int i = 0; i < array_len; i++){
        struct json_object *item = json_object_array_get_idx(parsed_json, i); // Untuk setiap objek di array json
        if(!json_object_is_type(item, json_type_object)){ // Mengecek apakah berupa tipe objek
            printf("Item not Found!\n");
            continue;
        }

        struct json_object *method, *song;
        if(!json_object_object_get_ex(item, "method", &method)){ // Mencari method dari objek tersebut
            printf("Encryption Not Found!\n");
            continue;
        }

        if(!json_object_object_get_ex(item, "song", &song)){ // Mencari song dari objek tersebut
            printf("Song Not Found!\n");
            continue;
        }

        char songName[json_object_get_string_len(song) + 1]; // Deklarasi songName sebagai tempat menyimpan song
        strcpy(songName, json_object_get_string(song)); // Mengcopy dari json ke songName

        if(strstr("rot13",json_object_get_string(method))) rot13_decode(songName); // Jika method rot13
        else if(strstr("hex", json_object_get_string(method))) hex_decode(songName); // Jika method hex
        else if(strstr("base64", json_object_get_string(method))) base64_decode(songName); // Jika method base64
        
        char cmd[strlen(songName) + 30];
        snprintf(cmd, sizeof(cmd), "echo \"%s\" >> playlist.txt", songName); // Membuat command untuk menambahkan ke playlist.txt
        system(cmd);
        printf("DECRYPTED:\nMethod: %s\nSong: %s\n\n", json_object_get_string(method), songName);
    }
    
    json_object_put(parsed_json); // Melepas json

    sort(); // Melakukan pengurutan di playlist.txt (Tanpa duplikat)
}

void add(int uid, char *in){
    char playlist[] = "playlist.txt";
    char add_arg[strlen(in)];
    strcpy(add_arg, in);
    
    if(findinplaylist(playlist, add_arg) < 1){ // Fungsi add perlu mencari secara eksak
        char cmd[strlen(add_arg) + 30];
        snprintf(cmd, sizeof(cmd), "echo \"%s\" >> playlist.txt", add_arg); // Jika tidak ditemukan maka input ditambah ke playlist.txt
        system(cmd);
        sort();

        printf("USER \"%d\" ADD %s\n", uid, add_arg);
    } else {
        printf("SONG ALREADY ON PLAYLIST\n");
    }
}

void play(int uid, char *in){
    char playlist[] = "playlist.txt";
    char play_arg[strlen(in) + 1];
    strcpy(play_arg, in);

    int found = 0;
    found = countinplaylist(playlist, play_arg); // Menghitung banyaknya line yang mengandung input di playlist.txt
    if(found == 0){
        printf("THERE IS NO SONG CONTAINING \"%s\"\n", play_arg); // Jika tidak ada input di playlist
    } else if(found == 1){ // Jika menemukan hanya satu di playlist
        FILE *fp;
        char temp[1024];
        char out[1025];

        if((fp = fopen(playlist, "r")) == NULL){
            return;
        }

        while(fgets(temp, sizeof(temp), fp) != NULL){
            if((strlen(temp) > 0) && (temp[strlen(temp) - 1] == '\n')) temp[strlen(temp) - 1] = '\0'; // Membandingkan apakah di line terdapat input (case insensitive)
            if((strcasestr(temp, play_arg)) != NULL) {
                strcpy(out, temp);
            }
        }

        if(fp){
            fclose(fp);
        }
        
        printf("USER \"%d\" PLAYING %s\n", uid, out);
    } else { // Jika ditemukan lebih dari satu
        printf("THERE ARE \"%d\" SONGS CONTAINING \"%s\"\n", found, play_arg);
        FILE *fp;
        int count = 1;
        char temp[1024];

        if((fp = fopen(playlist, "r")) == NULL){
            return;
        }

        while(fgets(temp, sizeof(temp), fp) != NULL){
            if(strcasestr(temp, play_arg) != NULL){ // Diprint dengan diberi urutan
                printf("%d. %s", count, temp);
                count++;
            }
        }

        if(fp){
            fclose(fp);
        }
    }

}

void list(){
    sort();
    char command[50];
    strcpy(command, "cat playlist.txt");
    system(command);
    printf("\n");
}

int main(){
    regex_t decrypt_regex, list_regex, add_regex, play_regex;
    
    const char *decrypt_pattern = "^DECRYPT\n$"; // Pola decrypt yang diperbolehkan
    const char *list_pattern = "^LIST\n$"; // Pola list yang diperbolehkan
    const char *add_pattern = "^ADD (.+)\n$"; // Pola add yang diperbolehkan
    const char *play_pattern = "^PLAY (.+)\n$"; // Pola play yang diperbolehkan
    
    int ret = regcomp(&decrypt_regex, decrypt_pattern, REG_EXTENDED); // Kompilasi regex
    ret |= regcomp(&list_regex, list_pattern, REG_EXTENDED);
    ret |= regcomp(&add_regex, add_pattern, REG_EXTENDED);
    ret |= regcomp(&play_regex, play_pattern, REG_EXTENDED);
    
    key_t key;
    int msgid;
  
    key = ftok("stream_playlist", 50);
  
    // setup semaphore
    sem_unlink("/user");
    sem_unlink("/stream");

    sem_t *sem_stream = sem_open("/stream", O_CREAT | O_EXCL, 0660, 0); // Semaphore sem_stream diinisialisasi 0
    if(sem_stream == SEM_FAILED){
        perror("sem_open/stream");
        exit(EXIT_FAILURE);
    }
    
    sem_t *sem_user = sem_open("/user", O_CREAT | O_EXCL, 0660, 2); // Semaphore sem_user diinisialisasi 2
    if(sem_user == SEM_FAILED){
        perror("sem_open/user");
        exit(EXIT_FAILURE);
    }
    
    msgid = msgget(key, 0666 | IPC_CREAT);
    
    while(1){
        
        sem_wait(sem_stream); // Menunggu pesan dari user

        msgrcv(msgid, &message, sizeof(message), 1, 0); // Menerima message
        
        printf("USER \"%d\" SENT DATA %s\n", message.user_pid, message.mesg_text); // Display message

        regmatch_t match[2];
        
        ret = regexec(&decrypt_regex, message.mesg_text, 0, NULL, 0); // Membandingkan input dengan regex
        if(ret == 0){ // Jika input berupa decrypt
            decrypt();
        } else {
            ret = regexec(&list_regex, message.mesg_text, 0, NULL, 0); // Jika input berupa list
            if(ret == 0) {
                list();
            } else { // Jika input berupa add
                ret = regexec(&add_regex, message.mesg_text, 2, match, 0);
                if(ret == 0){
                    char arg[match[1].rm_eo - match[1].rm_so + 1]; // Mengambil argumen dari input
                    memcpy(arg, message.mesg_text + match[1].rm_so, match[1].rm_eo - match[1].rm_so);
                    arg[match[1].rm_eo - match[1].rm_so] = '\0';
                    add(message.user_pid, arg);
                } else { // Jika input berupa play
                    ret = regexec(&play_regex, message.mesg_text, 2, match, 0);
                    if(ret == 0){
                        char arg[match[1].rm_eo - match[1].rm_so + 1]; // Mengambil argumen dari input
                        memcpy(arg, message.mesg_text + match[1].rm_so, match[1].rm_eo - match[1].rm_so);
                        arg[match[1].rm_eo - match[1].rm_so] = '\0';
                        play(message.user_pid, arg);
                    } else { // Error message
                        printf("UNKNOWN COMMAND!\n");
                    }
                }
            }
        }
        sem_post(sem_user); // Sinyal ke user bahwa message sudah diproses
    }
    return 0;
}