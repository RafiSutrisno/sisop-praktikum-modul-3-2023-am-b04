#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>

int main() {
  pid_t child_id;
  int status;

  child_id = fork();

  if (child_id < 0) {
    exit(EXIT_FAILURE);
  }

  if (child_id == 0) {
    char *argv[] = {"wget", "-O", "./hehe.zip","https://drive.google.com/uc?id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp&export=download", NULL};
    execv("/bin/wget", argv);
    char *mkdir[] = {"mkdir", "./hehe/", NULL};
    execv("/bin/mkdir", mkdir);
    exit(0);
  } else {
    wait(NULL);
    char *argv1[] = {"unzip", "hehe.zip", "-d", "./hehe/",  NULL};
    execv("/bin/unzip", argv1); 
    }
}