#include <stdio.h>
#include <string.h>
#include <stdlib.h>

struct Pair
{
    char *str;
    int num;
};

int main()
{
    const char *filePath = "log.txt";
    FILE *file = fopen(filePath, "r");

    char line[2051];
    int jml_acc = 0;

    // hitung ACCESSED
    while (fgets(line, sizeof(line), file) != NULL)
    {
        if (strstr(line, "ACCESSED") != NULL)
        {
            jml_acc++;
        }
    }

    printf("jumlah ACCESSED: %d\n", jml_acc);
    fseek(file, 0, SEEK_SET);


    printf("\nTOTAL PER FOLDER urut Ascending\n");
    char **folders = NULL;
    int folder_count = 0;
    while (fgets(line, sizeof(line), file) != NULL)
    {
        if (strstr(line, "MADE") != NULL)
        {
            char *folder_name = line;
            int spaceCount = 0;
            while (*folder_name != '\0')
            {
                if (*folder_name == ' ')
                {
                    spaceCount++;
                    if (spaceCount == 3)
                    {
                        folder_name++;
                        break;
                    }
                }
                folder_name++;
            }

            char *end = strchr(folder_name, '\n');
            if (end != NULL)
            {
                *end = '\0';
            }

            folders = realloc(folders, (folder_count + 1) * sizeof(char *));
            folders[folder_count] = strdup(folder_name);
            folder_count++;
        }
    }

    int total[folder_count];
    int arcount[folder_count];
    for (int i = 0; i < folder_count; i++)
    {

        fseek(file, 0, SEEK_SET);
        int count = 0;

        while (fgets(line, sizeof(line), file) != NULL)
        {
            if (strstr(line, "MOVE") != NULL && strstr(line, folders[i]) != NULL)
            {
                count++;
            }
        }
		arcount[i] = count;
    }
    for(int i = 0; i <= 10; i++){
    	for(int j = 0; j < folder_count; j++){
    		if(arcount[j] == i){
    			printf("%s: %d\n", folders[j], arcount[j]);
			}
		}
	}
	
	char arexten[7][4] = {"emc", "jpg", "js", "png", "py", "txt", "xyz"};
	int arex[7];
	while (fgets(line, sizeof(line), file) != NULL)
    {
        if (strstr(line, "MOVE") != NULL && strstr(line, "/emc") != NULL && strstr(line, "/other") == NULL)
        {
            arex[0]++;
        }else if (strstr(line, "MOVE") != NULL && strstr(line, "/jpg") != NULL && strstr(line, "/other") == NULL)
        {
            arex[1]++;
        }else if (strstr(line, "MOVE") != NULL && strstr(line, "/js") != NULL && strstr(line, "/other") == NULL)
        {
            arex[2]++;
        }else if (strstr(line, "MOVE") != NULL && strstr(line, "/png") != NULL && strstr(line, "/other") == NULL)
        {
            arex[3]++;
        }else if (strstr(line, "MOVE") != NULL && strstr(line, "/py") != NULL && strstr(line, "/other") == NULL)
        {
            arex[4]++;
        }else if (strstr(line, "MOVE") != NULL && strstr(line, "/txt") != NULL && strstr(line, "/other") == NULL)
        {
            arex[5]++;
        }else if (strstr(line, "MOVE") != NULL && strstr(line, "/xyz") != NULL && strstr(line, "/other") == NULL)
        {
            arex[6]++;
        }
    }
    printf("\n\n");
	for(int i = 0; i < 75; i++){
		for(int j = 0; j < 7; j++){
			if(arex[j] == i){
				printf("%s : %d\n", arexten[j], arex[j]);
			}
		}
	}

    fclose(file);
    return 0;
}

