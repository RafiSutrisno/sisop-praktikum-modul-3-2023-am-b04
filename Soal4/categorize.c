#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <wait.h>
#include <string.h>
#include <time.h>
#include <dirent.h>
#include <pthread.h>
#include <ctype.h>

char absoluteHehe[100];
char absoluteCategorized[100];
char absoluteLog[100];
char absoluteMax[100];
int maxFile;

//0: emc; 1: jpg; 2: js; 3: png; 4: py; 5: txt; 6: xyz; 7: other
int foldIdx[9]= {0, 0, 0, 0, 0, 0, 0};
int numFile[9]= {0, 0, 0, 0, 0, 0, 0};

void logCreate(char *dirName){
    FILE *log = fopen(absoluteLog, "a");

    time_t rawtime;
    char times[150];

    time(&rawtime);
    strftime(times, 80, "%d-%m-%Y %H:%M:%S", localtime(&rawtime));
    strcat(times, " MADE ");
    strcat(times, dirName);
    fprintf(log, "%s\n", times);
    fclose(log);
}

void logCreateAcc(char *Args){
    FILE *log = fopen(absoluteLog, "a");

    time_t rawtime;
    char times[1001];

    time(&rawtime);
    strftime(times, 80, "%d-%m-%Y %H:%M:%S", localtime(&rawtime));
    strcat(times, " ACCESSED ");
    strcat(times, Args);
    fprintf(log, "%s\n", times);
    fclose(log);
}

void logCreateMov(char *src, char *dest){
    FILE *log = fopen(absoluteLog, "a");

    time_t rawtime;
    char times[1001];

    time(&rawtime);
    strftime(times, 80, "%d-%m-%Y %H:%M:%S", localtime(&rawtime));
    strcat(times, " MOVE ");
    strcat(times, src);
    strcat(times, " > ");
    strcat(times, dest);
    fprintf(log, "%s\n", times);
    fclose(log);
}

void cek_jumlahFile_folder(char *folderext, char *type, int idx){
	DIR *dir2;
    struct dirent *ent;
    int count = 0;

    printf("move to: %s\n", folderext);
    dir2 = opendir(folderext);
    if (dir2 == NULL) {
        perror("opendir");
        pthread_exit(NULL);
	}
	else{
        while ((ent = readdir(dir2)) != NULL) {
            if (ent->d_type == DT_REG){
                count++;
            }
        }
        if(count > maxFile){
            foldIdx[idx]++;
            char newFold[20];
            sprintf(newFold, "%s %d", type, foldIdx[idx]);
            mkdir(newFold, 0700);
            logCreate(newFold);
        }
        closedir(dir2);
        printf("success\n");
    }
}

void move_file(char *src, char *filename){
    char dst[1001] = "";
    strcpy(dst, absoluteCategorized);
    char *extension = strrchr(filename, '.');

    char dstNonFile[2001];
    
    char foldDest[101];
    if(extension == NULL){
        // file "jangan dibuka" extension nya null tapi file
        // move ke other
        printf(" is a null other file\n");

        sprintf(foldDest, "other %d", foldIdx[7]);
        strcat(dst, foldDest);
        strcpy(dstNonFile, dst);
        strcat(dst, "/");
        strcat(dst, filename);

        rename(src, dst);
        cek_jumlahFile_folder(dstNonFile, "other", 7);
        logCreateMov(src, dst);
        numFile[7]++;
    }
    else{
        for(int i=0; i< strlen(extension); i++){
            extension[i] = tolower(extension[i]);
    	}
    	//printf("%s\n", filename);
    	char command[5001];
        //cek extension
        if(extension != NULL && strcmp(extension+1, "emc")==0){
        	// move ke folder emc
            printf(" is a emc file\n");

            sprintf(foldDest, "emc %d", foldIdx[0]);
            strcat(dst, foldDest);
            strcpy(dstNonFile, dst);
            strcat(dst, "/");
            strcat(dst, filename);

        	rename(src, dst);
            cek_jumlahFile_folder(dstNonFile, "emc", 0);
            logCreateMov(src, dst);
            numFile[0]++;		    
        }
        else if(extension != NULL && strcmp(extension+1, "jpg")==0){
            // move ke folder jpg
            printf(" is a jpg file\n");

            sprintf(foldDest, "jpg %d", foldIdx[1]);
            strcat(dst, foldDest);
            strcpy(dstNonFile, dst);
            strcat(dst, "/");
            strcat(dst, filename);

        	rename(src, dst);
            cek_jumlahFile_folder(dstNonFile, "jpg", 1);
            logCreateMov(src, dst);
            numFile[1]++;
        }
        else if(extension != NULL && strcmp(extension+1, "js")==0){
            // move ke folder js
            printf(" is a js file\n");

            sprintf(foldDest, "js %d", foldIdx[2]);
            strcat(dst, foldDest);
            strcpy(dstNonFile, dst);
            strcat(dst, "/");
            strcat(dst, filename);

        	rename(src, dst);
            cek_jumlahFile_folder(dstNonFile, "js", 2);
            logCreateMov(src, dst);
            numFile[2]++;
        }
        else if(extension != NULL && strcmp(extension+1, "png")==0){
            // move ke folder png
            printf(" is a png file\n");

            sprintf(foldDest, "png %d", foldIdx[3]);
            strcat(dst, foldDest);
            strcpy(dstNonFile, dst);
            strcat(dst, "/");
            strcat(dst, filename);

        	rename(src, dst);
            cek_jumlahFile_folder(dstNonFile, "png", 3);
            logCreateMov(src, dst);
            numFile[3]++;
        }
        else if(extension != NULL && strcmp(extension+1, "py")==0){
            // move ke folder py
            printf(" is a py file\n");

            sprintf(foldDest, "py %d", foldIdx[4]);
            strcat(dst, foldDest);
            strcpy(dstNonFile, dst);
            strcat(dst, "/");
            strcat(dst, filename);

        	rename(src, dst);
            cek_jumlahFile_folder(dstNonFile, "py", 4);
            logCreateMov(src, dst);
            numFile[4]++;
        }
        else if(extension != NULL && strcmp(extension+1, "txt")==0){
            // move ke folder txt
            printf(" is a txt file\n");

            sprintf(foldDest, "txt %d", foldIdx[5]);
            strcat(dst, foldDest);
            strcpy(dstNonFile, dst);
            strcat(dst, "/");
            strcat(dst, filename);

        	rename(src, dst);
            cek_jumlahFile_folder(dstNonFile, "txt", 5);
            logCreateMov(src, dst);
            numFile[5]++;
        }
        else if(extension != NULL && strcmp(extension+1, "xyz")==0){
            // move ke folder xyz
            printf(" is a xyz file\n");

            sprintf(foldDest, "xyz %d", foldIdx[6]);
            strcat(dst, foldDest);
            strcpy(dstNonFile, dst);
            strcat(dst, "/");
            strcat(dst, filename);

        	rename(src, dst);
            cek_jumlahFile_folder(dstNonFile, "xyz", 6);
            logCreateMov(src, dst);
            numFile[6]++;
        }
        else{
            // move ke folder other
            printf(" is a non-Null other file\n");

            sprintf(foldDest, "other %d", foldIdx[7]);
            strcat(dst, foldDest);
            strcpy(dstNonFile, dst);
            strcat(dst, "/");
            strcat(dst, filename);

            rename(src, dst);
            cek_jumlahFile_folder(dstNonFile, "other", 7);
            logCreateMov(src, dst);
            numFile[7]++;  
        }
    }
    printf("\n");

}

void *thread_dir_list(void *arg) {
    char *path = (char *) arg;
    DIR *dir;
    struct dirent *entry;

    // Open directory
    dir = opendir(path);
    if (dir == NULL) {
        perror("opendir");
        pthread_exit(NULL);
    }

    // Loop over directory entries
    while ((entry = readdir(dir)) != NULL) {
        char fullpath[1001];

        if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) {
            // Skip current and parent directory entries
            continue;
        }

        // Print entry name
        printf("%s/%s\n", path, entry->d_name);

        if (entry->d_type == DT_DIR) {
            // Entry is a directory, create new thread to list it
			logCreateAcc(entry->d_name);
            pthread_t tid;
            sprintf(fullpath, "%s/%s", path, entry->d_name);

            if (pthread_create(&tid, NULL, thread_dir_list, fullpath) != 0) {
                perror("pthread_create");
                continue;
            }

            // Wait for thread to finish
            if (pthread_join(tid, NULL) != 0) {
                perror("pthread_join");
                continue;
            }
        } else {
            // Entry is a file
            char filePath[1001];
            strcpy(filePath, path);
            strcat(filePath, "/");
            strcat(filePath, entry->d_name);

            move_file(filePath, entry->d_name);
        }
    }

    closedir(dir);
    pthread_exit(NULL);
}




int getMax(){
    int res;
    FILE *max = fopen(absoluteMax, "r");
    fscanf(max, "%d", &res);
    fclose(max);
    return res;
}


int main(){
    char cwd[101];
    getcwd(cwd, sizeof(cwd));

    strcpy(absoluteLog, cwd);
    strcat(absoluteLog, "/log.txt");
    strcpy(absoluteMax, cwd);
    strcat(absoluteMax, "/hehe/max.txt");
    strcpy(absoluteHehe, cwd);
    strcat(absoluteHehe, "/hehe/files");
    strcpy(absoluteCategorized, cwd);
    strcat(absoluteCategorized, "/categorized/");

    strcat(cwd, "/hehe/extensions.txt");

    maxFile = getMax()-1;

    FILE *ext;
    ext = fopen(cwd, "r");

    mkdir("categorized", 0700);
    logCreate("categorized");
    
    chdir("categorized");
    char dirname[10];
    while(fscanf(ext, "%s", dirname) == 1){
        strcat(dirname, " 0");
        mkdir(dirname, 0700);
        logCreate(dirname);
    }
    mkdir("other 0", 0700);
    logCreate("other 0");

    pthread_t tid;
	logCreateAcc("files");
    if (pthread_create(&tid, NULL, thread_dir_list, absoluteHehe) != 0) {
        perror("pthread_create");
        exit(EXIT_FAILURE);
    }
    // Wait for thread to finish
    if (pthread_join(tid, NULL) != 0) {
        perror("pthread_join");
        exit(EXIT_FAILURE);
    }
    printf("emc: %d\njpg: %d\njs: %d\npng: %d\npy: %d\ntxt: %d\nxyz: %d\nother: %d\n", numFile[0],numFile[1],numFile[2],numFile[3],numFile[4],numFile[5],numFile[6],numFile[7]);

    return 0;
}

